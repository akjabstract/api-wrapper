<?php namespace AKJAbstract\APIWrapper\Requests\CLIs;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Requests\AbstractRequest;
use AKJAbstract\APIWrapper\Requests\AbstractRequestInterface;

class CLIUpdateRequest extends AbstractRequest implements AbstractRequestInterface
{
    protected $request_array;

    protected $required_fields = [];

    public function __construct()
    {
        $this->request_array['UserFields'] = [];
    }

    public function live(bool $live)
    {
        $this->request_array['Live'] = ($live) ? 1 : 0;

        return $this;
    }

    public function carrier_1(string $carrier_1)
    {
        $this->request_array['Carrier1'] = $carrier_1;

        return $this;
    }

    public function site_id(int $site_id)
    {
        $this->request_array['SiteID'] = $site_id;

        return $this;
    }

    public function sub_site_id(int $sub_site_id)
    {
        $this->request_array['SubSiteID'] = $sub_site_id;

        return $this;
    }
}