<?php namespace AKJAbstract\APIWrapper\Requests;

use Carbon\Carbon;

abstract class AbstractRequest
{
    protected $request_array;

    protected $required_fields = [];

    /**
     * Check to see if request is valid
     * @return bool
     */
    public function isValid()
    {
        foreach ($this->required_fields as $field) {
            if ($this->request_array[$field] == null) return false;
        }

        return true;
    }

    /**
     * get the errors if not valid
     * @return array
     */
    public function getErrors()
    {
        $errors = [];

        foreach ($this->required_fields as $field) {
            if ($this->request_array[$field] == null) $errors[] = 'Missing key field: ' . $field;
        }

        return $errors;
    }

    /**
     * Get the requset array
     * @return array
     */
    public function getRequestArray(): array
    {
        return $this->request_array;
    }

    /**
     * Get field from the request array
     * @param string $field
     * @return mixed
     */
    public function getField(string $field)
    {
        return $this->request_array[$field];
    }

    /**
     * Returns the requested user field
     * @param string $field_name
     * @return mixed
     */
    public function getUserField(string $field_name)
    {
        return $this->request_array['UserFields'][$field_name];
    }

    /**
     * Sets the userfield with the value, use as generic
     * @param string $field_name
     * @param $value
     * @return $this
     */
    public function userField(string $field_name, $value)
    {
        $this->request_array['UserFields'][$field_name] = $value;

        return $this;
    }
}