<?php namespace AKJAbstract\APIWrapper\Requests\Associations;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Requests\AbstractRequest;
use AKJAbstract\APIWrapper\Requests\AbstractRequestInterface;

class AssociationCreateRequest extends AbstractRequest implements AbstractRequestInterface
{
    protected $request_array;

    protected $required_fields = ['ItemUID','ItemConnectionType','AssociatedUID','AssociatedConnectionType'];

    public function __construct()
    {
        $this->request_array['ItemUID'] = null;
        $this->request_array['AssociatedUID'] = null;
        $this->request_array['ItemConnectionType'] = null;
        $this->request_array['AssociatedConnectionType'] = null;
    }

    public function itemUid(string $itemUID)
    {
        $this->request_array['ItemUID'] = $itemUID;

        return $this;
    }

    public function associatedUID(string $associatedUID)
    {
        $this->request_array['AssociatedUID'] = $associatedUID;

        return $this;
    }

    public function itemConnectionType(string $type)
    {
        $this->request_array['ItemConnectionType'] = $type;

        return $this;
    }

    public function associationConnectionType(string $type)
    {
        $this->request_array['AssociatedConnectionType'] = $type;

        return $this;
    }

}