<?php namespace AKJAbstract\APIWrapper\Requests\SubSites;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Requests\AbstractRequest;
use AKJAbstract\APIWrapper\Requests\AbstractRequestInterface;

class SubSiteUpdateRequest extends AbstractRequest implements AbstractRequestInterface
{
    protected $request_array;

    protected $required_fields = [];

    public function __construct()
    {
        $this->request_array['SubSiteName'] = null;
        $this->request_array['AddressID'] = null;
        $this->request_array['SubSiteRef1'] = null;
        $this->request_array['SubSiteRef2'] = null;
        $this->request_array['Live'] = null;
    }

    public function sub_site_name(string $sub_site_name)
    {
        $this->request_array['SubSiteName'] = $sub_site_name;

        return $this;
    }

    public function address_id(int $address_id)
    {
        $this->request_array['AddressID'] = $address_id;

        return $this;
    }

    public function sub_site_ref_1(string $sub_site_ref_1)
    {
        $this->request_array['SubSiteRef1'] = $sub_site_ref_1;

        return $this;
    }

    public function sub_site_ref_2(string $sub_site_ref_2)
    {
        $this->request_array['ResidentialCustomer'] = $sub_site_ref_2;

        return $this;
    }

    public function live(int $live)
    {
        $this->request_array['Live'] = $live;

        return $this;
    }
}