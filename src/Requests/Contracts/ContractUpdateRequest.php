<?php namespace AKJAbstract\APIWrapper\Requests\Contracts;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Requests\AbstractRequest;
use AKJAbstract\APIWrapper\Requests\AbstractRequestInterface;

class ContractUpdateRequest extends AbstractRequest implements AbstractRequestInterface
{
    protected $request_array = [];

    protected $required_fields = [];

    public function __construct()
    {
    }

    /**
     * Check to see if request is valid, in an update any field is required
     * @return bool
     */
    public function isValid()
    {
        return true;
    }

    public function description(string $description)
    {
        $this->request_array['Description'] = $description;

        return $this;
    }

    public function contractTerm(int $contract_term)
    {
        $this->request_array['ContractTerm'] = $contract_term;

        return $this;
    }

    public function startDate(Carbon $start_date)
    {
        $this->request_array['StartDate'] = $start_date->toW3cString();

        return $this;
    }

    public function endDate(Carbon $end_date)
    {
        $this->request_array['EndDate'] = $end_date->toW3cString();

        return $this;
    }

    public function contractType(string $contract_type)
    {
        $this->request_array['ContractType'] = $contract_type;

        return $this;
    }

    public function status(string $status)
    {
        $this->request_array['Status'] = $status;

        return $this;
    }


    public function noticePeriod(int $notice_period)
    {
        $this->request_array['NoticePeriod'] = $notice_period;

        return $this;
    }


    public function reference(string $reference)
    {
        $this->request_array['Reference'] = $reference;

        return $this;
    }


    public function terminationDate(Carbon $termination_date)
    {
        $this->request_array['TerminationDate'] = $termination_date->toW3cString();

        return $this;
    }

    public function signedDate(Carbon $signed_date)
    {
        $this->request_array['SignedDate'] = $signed_date->toW3cString();

        return $this;
    }

    public function signedBy(string $signed_by)
    {
        $this->request_array['SignedBy'] = $signed_by;

        return $this;
    }

    public function noticeReceived(Carbon $notice_received_date)
    {
        $this->request_array['NoticeReceived'] = $notice_received_date->toW3cString();

        return $this;
    }

    public function noticeGivenBy(string $notice_received_by)
    {
        $this->request_array['NoticeGivenBy'] = $notice_received_by;

        return $this;
    }

    public function connectedTo(string $connected_to)
    {
        $this->request_array['ConnectedTo'] = $connected_to;

        return $this;
    }

    public function connectionType(string $connected_type)
    {
        $this->request_array['ConnectionType'] = $connected_type;

        return $this;
    }

}