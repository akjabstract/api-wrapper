<?php namespace AKJAbstract\APIWrapper\Requests;

use Carbon\Carbon;

interface AbstractRequestInterface
{
    public function getRequestArray();

    public function isValid();

    public function getErrors();
}