<?php namespace AKJAbstract\APIWrapper\Requests\Sites;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Requests\AbstractRequest;
use AKJAbstract\APIWrapper\Requests\AbstractRequestInterface;

class SiteUpdateRequest extends AbstractRequest implements AbstractRequestInterface
{
    protected $request_array;

    protected $required_fields = [];

    public function __construct()
    {
        $this->request_array['CycleID'] = null;
        $this->request_array['BSA'] = null;
        $this->request_array['SiteName'] = null;
        $this->request_array['PaymentMethod'] = null;
        $this->request_array['Live'] = null;
        $this->request_array['SiteContactID'] = null;
        $this->request_array['CompanyID'] = null;
        $this->request_array['ResidentialCustomer'] = null;
        $this->request_array['PaymentDetails'] = null;
        $this->request_array['SiteFlags'] = null;
        $this->request_array['ContractLength'] = null;

        // Userfields did not work on test system
        //$this->request_array['UserFields'] = [];
    }

    public function live(int $live)
    {
        $this->request_array['Live'] = $live;

        return $this;
    }

    public function site_contact_id(int $site_contact_id)
    {
        $this->request_array['SiteContactID'] = $site_contact_id;

        return $this;
    }

    public function company_id(int $company_id)
    {
        $this->request_array['CompanyID'] = $company_id;

        return $this;
    }

    public function residential_customer(bool $residential_customer)
    {
        $this->request_array['ResidentialCustomer'] = ($residential_customer) ? 1 : 0;

        return $this;
    }

    public function cycle_id(int $cycle_id)
    {
        $this->request_array['CycleID'] = $cycle_id;

        return $this;
    }

    public function bsa(string $bsa)
    {
        $this->request_array['BSA'] = $bsa;

        return $this;
    }

    public function site_name(string $site_name)
    {
        $this->request_array['SiteName'] = $site_name;

        return $this;
    }

    public function payment_method(string $payment_method)
    {
        $this->request_array['PaymentMethod'] = $payment_method;

        return $this;
    }

    public function payment_terms(?int $payment_terms, ?string $bank_name = null)
    {
        if ($this->request_array['PaymentDetails'] === null) $this->request_array['PaymentDetails'] = [];

        if($payment_terms) $this->request_array['PaymentDetails']['PaymentTerms'] = $payment_terms;

        if($bank_name) $this->request_array['PaymentDetails']['BankName'] = $bank_name;

        return $this;
    }

    public function site_flags(array $site_flags)
    {
        if ($this->request_array['SiteFlags'] === null) $this->request_array['SiteFlags'] = [];

        if(isset($site_flags['email_only_bill'])) $this->request_array['SiteFlags']['EmailOnlyBill'] = $site_flags['email_only_bill'];

        return $this;
    }

    public function contract_length(int $contract_length)
    {
        $this->request_array['ContractLength'] = $contract_length;

        return $this;
    }
}