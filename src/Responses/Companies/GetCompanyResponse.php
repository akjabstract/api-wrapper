<?php namespace AKJAbstract\APIWrapper\Responses\Companies;

use AKJAbstract\APIWrapper\Objects\AffinityCompanyObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetCompanyResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityCompanyObject;

    public function __construct()
    {
        $this->affinityCompanyObject = new AffinityCompanyObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityCompanyObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityCompanyObject
    {
        return $this->affinityCompanyObject;
    }
}