<?php namespace AKJAbstract\APIWrapper\Responses\EmailAddresses;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinityEmailAddressObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetEmailAddressResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityEmailAddressObject;

    public function __construct()
    {
        $this->affinityEmailAddressObject = new AffinityEmailAddressObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityEmailAddressObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityEmailAddressObject
    {
        return $this->affinityEmailAddressObject;
    }
}