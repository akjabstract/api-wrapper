<?php namespace AKJAbstract\APIWrapper\Responses\Sites;

use AKJAbstract\APIWrapper\Objects\AffinitySiteBroadbandObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetSiteBroadbandResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinitySiteBroadbandObject;

    public function __construct()
    {
        $this->affinitySiteBroadbandObject = new AffinitySiteBroadbandObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinitySiteBroadbandObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinitySiteBroadbandObject
    {
        return $this->affinitySiteBroadbandObject;
    }
}