<?php namespace AKJAbstract\APIWrapper\Responses\Sites;

use AKJAbstract\APIWrapper\Objects\AffinitySiteBroadbandObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetSiteBroadbandsResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinitySiteBroadbandObject;

    protected $affinity_site_broadband_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_site_broadband_objects = [];

        if(isset($affinity_response->SiteBroadbands->SiteBroadband) && is_array($affinity_response->SiteBroadbands->SiteBroadband)){
            foreach($affinity_response->SiteBroadbands->SiteBroadband as $affinity_site_broadband){
                $this->appendObject($affinity_site_broadband);
            }
        }elseif(isset($affinity_response->SiteBroadbands->SiteBroadband) && is_object($affinity_response->SiteBroadbands->SiteBroadband)){
            $this->appendObject($affinity_response->SiteBroadbands->SiteBroadband);
        }elseif(isset($affinity_response->SiteBroadband)){
            $this->appendObject($affinity_response->SiteBroadband);
        }

        return $this;
    }

    protected function appendObject($affinity_site_broadband){
        $affinity_site_object = new AffinitySiteBroadbandObject();

        $affinity_site_object->setObject($affinity_site_broadband);

        $this->affinity_site_broadband_objects[] = $affinity_site_object;
    }


    public function getResponse(): array
    {
        return $this->affinity_site_broadband_objects;
    }
}