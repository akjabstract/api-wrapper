<?php namespace AKJAbstract\APIWrapper\Responses\Addresses;

use AKJAbstract\APIWrapper\Objects\AffinityAddressObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetAddressResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityAddressObject;

    public function __construct()
    {
        $this->affinityAddressObject = new AffinityAddressObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityAddressObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityAddressObject
    {
        return $this->affinityAddressObject;
    }
}