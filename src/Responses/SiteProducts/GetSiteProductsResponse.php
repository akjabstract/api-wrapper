<?php namespace AKJAbstract\APIWrapper\Responses\SiteProducts;

use AKJAbstract\APIWrapper\Objects\AffinitySiteProductObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetSiteProductsResponse extends AbstractResponse implements ResponseInterface
{

    protected $affinity_site_product_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_site_product_objects = [];

        if(isset($affinity_response->SiteProductItems->SiteProductItem) && is_array($affinity_response->SiteProductItems->SiteProductItem)){
            foreach($affinity_response->SiteProductItems->SiteProductItem as $affinity_site_product){
                $this->appendObject($affinity_site_product);
            }
        }elseif(isset($affinity_response->SiteProductItems->SiteProduct) && is_object($affinity_response->SiteProductItems->SiteProductItem)){
            $this->appendObject($affinity_response->SiteProductItems->SiteProductItem);
        }elseif(isset($affinity_response->SiteProductItem)){
            $this->appendObject($affinity_response->SiteProductItem);
        }

        return $this;
    }

    protected function appendObject($affinity_site_product){
        $affinity_site_product_object = new AffinitySiteProductObject();

        $affinity_site_product_object->setObject($affinity_site_product);

        $this->affinity_site_product_objects[] = $affinity_site_product_object;
    }


    public function getResponse(): array
    {
        return $this->affinity_site_product_objects;
    }
}