<?php namespace AKJAbstract\APIWrapper\Responses\SiteProducts;

use AKJAbstract\APIWrapper\Objects\AffinitySiteProductObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetSiteProductResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinitySiteProductObject;

    public function __construct()
    {
        $this->affinitySiteProductObject = new AffinitySiteProductObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinitySiteProductObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinitySiteProductObject
    {
        return $this->affinitySiteProductObject;
    }
}