<?php namespace AKJAbstract\APIWrapper\Responses\Tariffs;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinityTariffObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetTariffsResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityTariffObject;

    protected $affinity_tariff_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_tariff_objects = [];

        if(isset($affinity_response->ProductTariffSchemes->ProductTariffScheme)){
            foreach($affinity_response->ProductTariffSchemes->ProductTariffScheme as $affinity_tariff){
                $this->appendObject($affinity_tariff);
            }
        }elseif(isset($affinity_response->ProductTariffScheme)){
           $this->appendObject($affinity_response->ProductTariffScheme);
        }

        return $this;
    }

    protected function appendObject($affinity_tariff){
        $affinity_tariff_object = new AffinityTariffObject();

        $affinity_tariff_object->setObject($affinity_tariff);

        $this->affinity_tariff_objects[] = $affinity_tariff_object;
    }

    public function getResponse(): array
    {
        return $this->affinity_tariff_objects;
    }
}