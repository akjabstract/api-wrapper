<?php namespace AKJAbstract\APIWrapper\Responses\Tariffs;

use AKJAbstract\APIWrapper\Objects\AffinityAssignedTariffObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetAssignedTariffsResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityTariffObject;

    protected $affinity_assigned_tariff_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_tariff_objects = [];

        if(isset($affinity_response->SiteProductTariffSchemes->SiteProductTariffScheme)){
            foreach($affinity_response->SiteProductTariffSchemes->SiteProductTariffScheme as $affinity_assigned_tariff){
                $this->appendObject($affinity_assigned_tariff);
            }
        }elseif(isset($affinity_response->SiteProductTariffScheme)){
            $this->appendObject($affinity_response->SiteProductTariffScheme);
        }

        return $this;
    }

    protected function appendObject($affinity_assigned_tariff){
        $affinity_tariff_object = new AffinityAssignedTariffObject();

        $affinity_tariff_object->setObject($affinity_assigned_tariff);

        $this->affinity_assigned_tariff_objects[] = $affinity_tariff_object;
    }


    public function getResponse(): array
    {
        return $this->affinity_assigned_tariff_objects;
    }
}