<?php namespace AKJAbstract\APIWrapper\Responses\Tariffs;

use AKJAbstract\APIWrapper\Objects\AffinityAssignedTariffObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetAssignedTariffResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityAssignedTariffObject;

    public function __construct()
    {
        $this->affinityAssignedTariffObject = new AffinityAssignedTariffObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityAssignedTariffObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityAssignedTariffObject
    {
        return $this->affinityAssignedTariffObject;
    }
}