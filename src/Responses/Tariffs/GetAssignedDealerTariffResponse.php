<?php namespace AKJAbstract\APIWrapper\Responses\Tariffs;

use AKJAbstract\APIWrapper\Objects\AffinityAssignedDealerTariffObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetAssignedDealerTariffResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityAssignedDealerTariffObject;

    public function __construct()
    {
        $this->affinityAssignedDealerTariffObject = new AffinityAssignedDealerTariffObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityAssignedDealerTariffObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityAssignedDealerTariffObject
    {
        return $this->affinityAssignedDealerTariffObject;
    }
}