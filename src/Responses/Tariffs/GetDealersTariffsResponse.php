<?php namespace AKJAbstract\APIWrapper\Responses\Tariffs;

use AKJAbstract\APIWrapper\Objects\AffinityDealerTariffObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetDealersTariffsResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinity_dealers_tariff_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_dealers_tariff_objects = [];

        if(isset($affinity_response->DealerTariffSchemes->DealerTariffScheme)){
            foreach($affinity_response->DealerTariffSchemes->DealerTariffScheme as $affinity_dealer_tariff){
                $this->appendObject($affinity_dealer_tariff);
            }
        }elseif(isset($affinity_response->DealerTariffScheme)){
           $this->appendObject($affinity_response->DealerTariffScheme);
        }

        return $this;
    }

    protected function appendObject($affinity_dealer_tariff){
        $affinity_tariff_object = new AffinityDealerTariffObject();

        $affinity_tariff_object->setObject($affinity_dealer_tariff);

        $this->affinity_dealers_tariff_objects[] = $affinity_tariff_object;
    }

    public function getResponse(): array
    {
        return $this->affinity_dealers_tariff_objects;
    }
}