<?php namespace AKJAbstract\APIWrapper\Responses\Tariffs;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinityTariffObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetTariffResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityTariffObject;

    public function __construct()
    {
        $this->affinityTariffObject = new AffinityTariffObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityTariffObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityTariffObject
    {
        return $this->affinityTariffObject;
    }
}