<?php namespace AKJAbstract\APIWrapper\Responses\Teams;

use AKJAbstract\APIWrapper\Objects\AffinityTeamObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetTeamResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityTeamObject;

    public function __construct()
    {
        $this->affinityTeamObject = new AffinityTeamObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityTeamObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityTeamObject
    {
        return $this->affinityTeamObject;
    }
}