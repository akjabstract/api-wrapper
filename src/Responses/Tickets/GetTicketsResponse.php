<?php namespace AKJAbstract\APIWrapper\Responses\Tickets;

use AKJAbstract\APIWrapper\Objects\AffinityTicketObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetTicketsResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityTicketObject;

    protected $affinity_ticket_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_ticket_objects = [];

        if(isset($affinity_response->Tickets->Ticket) && is_array($affinity_response->Tickets->Ticket)){
            foreach($affinity_response->Tickets->Ticket as $affinity_ticket){
                $this->appendObject($affinity_ticket);
            }
        }
        elseif(isset($affinity_response->Tickets->Ticket) && is_object($affinity_response->Tickets->Ticket)){
            $this->appendObject($affinity_response->Tickets->Ticket);
        }elseif(isset($affinity_response->Ticket)){
            $this->appendObject($affinity_response->Ticket);
        }

        return $this;
    }

    protected function appendObject($affinity_ticket){
        $affinity_ticket_object = new AffinityTicketObject();

        $affinity_ticket_object->setObject($affinity_ticket);

        $this->affinity_ticket_objects[] = $affinity_ticket_object;
    }


    public function getResponse(): array
    {
        return $this->affinity_ticket_objects;
    }
}