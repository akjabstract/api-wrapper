<?php namespace AKJAbstract\APIWrapper\Responses\Tickets;

use AKJAbstract\APIWrapper\Objects\AffinityTicketObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetTicketResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityTicketObject;

    public function __construct()
    {
        $this->affinityTicketObject = new AffinityTicketObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityTicketObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityTicketObject
    {
        return $this->affinityTicketObject;
    }
}