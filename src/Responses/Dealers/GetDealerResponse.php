<?php namespace AKJAbstract\APIWrapper\Responses\Dealers;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetDealerResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityDealerObject;

    public function __construct()
    {
        $this->affinityDealerObject = new AffinityCLIObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityDealerObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityCLIObject
    {
        return $this->affinityDealerObject;
    }
}