<?php namespace AKJAbstract\APIWrapper\Responses\Dealers;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetDealersResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityDealerObject;

    protected $affinity_dealer_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_dealer_objects = [];

        foreach($affinity_response->CLIs->CLI as $affinity_dealer){
            $affinity_dealer_object = new AffinityCLIObject();

            $affinity_dealer_object->setObject($affinity_dealer);

            $this->affinity_dealer_objects[] = $affinity_dealer_object;
        }

        return $this;
    }

    public function getResponse(): array
    {
        return $this->affinity_dealer_objects;
    }
}