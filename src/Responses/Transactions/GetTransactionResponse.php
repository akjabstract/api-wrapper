<?php namespace AKJAbstract\APIWrapper\Responses\Transactions;

use AKJAbstract\APIWrapper\Objects\AffinityTicketObject;
use AKJAbstract\APIWrapper\Objects\AffinityTransactionObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetTransactionResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityTransactionObject;

    public function __construct()
    {
        $this->affinityTransactionObject = new AffinityTransactionObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityTransactionObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityTransactionObject
    {
        return $this->affinityTransactionObject;
    }
}