<?php namespace AKJAbstract\APIWrapper\Responses\Transactions;

use AKJAbstract\APIWrapper\Objects\AffinityTicketObject;
use AKJAbstract\APIWrapper\Objects\AffinityTransactionObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetTransactionsResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinity_transaction_objects;

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_transaction_objects = [];

        if(isset($affinity_response->SalesLedgerTransactions->SalesLedgerTransactions) && is_array($affinity_response->SalesLedgerTransactions->SalesLedgerTransactions)){
            foreach($affinity_response->SalesLedgerTransactions->SalesLedgerTransactions as $affinity_transaction){
                $this->appendObject($affinity_transaction);
            }
        }
        elseif(isset($affinity_response->SalesLedgerTransactions->SalesLedgerTransactions) && is_object($affinity_response->SalesLedgerTransactions->SalesLedgerTransactions)){
            $this->appendObject($affinity_response->SalesLedgerTransactions->SalesLedgerTransactions);
        }elseif(isset($affinity_response->SalesLedgerTransactions)){
            $this->appendObject($affinity_response->SalesLedgerTransactions);
        }

        return $this;
    }

    protected function appendObject($affinity_transaction){
        $affinity_transaction_object = new AffinityTransactionObject();

        $affinity_transaction_object->setObject($affinity_transaction);

        $this->affinity_transaction_objects[] = $affinity_transaction_object;
    }


    public function getResponse(): array
    {
        return $this->affinity_transaction_objects;
    }
}