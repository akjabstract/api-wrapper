<?php namespace AKJAbstract\APIWrapper\Responses\Contracts;

use AKJAbstract\APIWrapper\Objects\AffinityContractItemObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetContractItemResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityContractItemObject;

    public function __construct()
    {
        $this->affinityContractItemObject = new AffinityContractItemObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityContractItemObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityContractItemObject
    {
        return $this->affinityContractItemObject;
    }
}