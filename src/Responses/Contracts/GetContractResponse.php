<?php namespace AKJAbstract\APIWrapper\Responses\Contracts;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinityContractObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetContractResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityContractObject;

    public function __construct()
    {
        $this->affinityContractObject = new AffinityContractObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityContractObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityContractObject
    {
        return $this->affinityContractObject;
    }
}