<?php namespace AKJAbstract\APIWrapper\Responses\Contracts;

use AKJAbstract\APIWrapper\Objects\AffinityContractObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetContractsResponse extends AbstractResponse implements ResponseInterface
{

    protected $affinity_contract_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_contract_objects = [];

        if(isset($affinity_response->Contracts->Contract)){
            foreach($affinity_response->Contracts->Contract as $affinity_contract){
                $this->appendObject($affinity_contract);
            }
        }elseif(isset($affinity_response->Contract)){
            $this->appendObject($affinity_response->Contract);
        }

        return $this;
    }

    protected function appendObject($affinity_contract){
        $affinityContractObject = new AffinityContractObject();

        $affinityContractObject->setObject($affinity_contract);

        $this->affinity_contract_objects[] = $affinityContractObject;
    }

    public function getResponse(): array
    {
        return $this->affinity_contract_objects;
    }
}