<?php namespace AKJAbstract\APIWrapper\Responses\Contracts;

use AKJAbstract\APIWrapper\Objects\AffinityContractItemObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetContractItemsResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityContractItemObject;

    protected $affinity_contract_item_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_contract_item_objects = [];

        if(isset($affinity_response->ContractItems->ContractItem)){
            foreach($affinity_response->ContractItems->ContractItem as $affinity_contract_item){
                $this->appendObject($affinity_contract_item);
            }
        }elseif(isset($affinity_response->ContractItem)){
            $this->appendObject($affinity_response->ContractItem);
        }

        return $this;
    }

    protected function appendObject($affinity_contract_item){
        $affinity_tariff_object = new AffinityContractItemObject();

        $affinity_tariff_object->setObject($affinity_contract_item);

        $this->affinity_contract_item_objects[] = $affinity_tariff_object;
    }

    public function getResponse(): array
    {
        return $this->affinity_contract_item_objects;
    }
}