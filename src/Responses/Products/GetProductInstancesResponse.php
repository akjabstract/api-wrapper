<?php namespace AKJAbstract\APIWrapper\Responses\Products;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinityProductObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetProductInstancesResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityProductInstanceObject;

    protected $affinity_product_instances_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_product_instances_objects = [];

        if(isset($affinity_response->SiteProductItems->SiteProductItem)){
            foreach($affinity_response->SiteProductItems->SiteProductItem as $affinity_product_instance){
                $this->appendObject($affinity_product_instance);
            }
        }elseif(isset($affinity_response->SiteProductItem)){
            $this->appendObject($affinity_response->SiteProductItem);
        }

        return $this;
    }

    protected function appendObject($affinity_product_instance){
        $affinity_product_instance_object = new AffinityProductObject();

        $affinity_product_instance_object->setObject($affinity_product_instance);

        $this->affinity_product_instances_objects[] = $affinity_product_instance_object;
    }

    public function getResponse(): array
    {
        return $this->affinity_product_instances_objects;
    }
}