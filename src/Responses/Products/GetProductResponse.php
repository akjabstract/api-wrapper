<?php namespace AKJAbstract\APIWrapper\Responses\Products;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinityNoteObject;
use AKJAbstract\APIWrapper\Objects\AffinityProductObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetProductResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityProductObject;

    public function __construct()
    {
        $this->affinityProductObject = new AffinityProductObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityProductObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityProductObject
    {
        return $this->affinityProductObject;
    }
}