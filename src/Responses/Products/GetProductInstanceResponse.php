<?php namespace AKJAbstract\APIWrapper\Responses\Products;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinityNoteObject;
use AKJAbstract\APIWrapper\Objects\AffinityProductInstanceObject;
use AKJAbstract\APIWrapper\Objects\AffinityProductObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetProductInstanceResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityProductInstanceObject;

    public function __construct()
    {
        $this->affinityProductInstanceObject = new AffinityProductInstanceObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityProductInstanceObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityProductInstanceObject
    {
        return $this->affinityProductInstanceObject;
    }
}