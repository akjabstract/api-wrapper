<?php namespace AKJAbstract\APIWrapper\Responses\Products;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinityNoteObject;
use AKJAbstract\APIWrapper\Objects\AffinityProductCategoryObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetProductCategoryResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityCategoryObject;

    public function __construct()
    {
        $this->affinityCategoryObject = new AffinityProductCategoryObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityCategoryObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityProductCategoryObject
    {
        return $this->affinityCategoryObject;
    }
}