<?php namespace AKJAbstract\APIWrapper\Responses\Products;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinityProductCategoryObject;
use AKJAbstract\APIWrapper\Objects\AffinityProductObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetProductCategoriesResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityProductCategoryObject;

    protected $affinity_product_category_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_product_category_objects = [];

        if(isset($affinity_response->ProductItemCategories->ProductItemCategory)){
            foreach($affinity_response->ProductItemCategories->ProductItemCategory as $affinity_product_category){
                $this->appendObject($affinity_product_category);
            }
        }elseif(isset($affinity_response->Site)){
            $this->appendObject($affinity_response->ProductItemCategory);
        }

        return $this;
    }

    protected function appendObject($affinity_product_category){
        $affinity_product_category_object = new AffinityProductCategoryObject();

        $affinity_product_category_object->setObject($affinity_product_category);

        $this->affinity_product_category_objects[] = $affinity_product_category_object;
    }

    public function getResponse(): array
    {
        return $this->affinity_product_category_objects;
    }
}