<?php namespace AKJAbstract\APIWrapper\Responses\Contacts;

use AKJAbstract\APIWrapper\Objects\AffinityCompanyObject;
use AKJAbstract\APIWrapper\Objects\AffinityContactObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetContactResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityContactObject;

    public function __construct()
    {
        $this->affinityContactObject = new AffinityContactObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityContactObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityContactObject
    {
        return $this->affinityContactObject;
    }
}