<?php namespace AKJAbstract\APIWrapper\Responses\Associations;

use AKJAbstract\APIWrapper\Objects\AffinityAssociationsObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetAssociationResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityAssociationObject;

    public function __construct()
    {
        $this->affinityAssociationObject = new AffinityAssociationsObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityAssociationObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityAssociationsObject
    {
        return $this->affinityAssociationObject;
    }
}