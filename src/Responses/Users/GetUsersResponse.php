<?php namespace AKJAbstract\APIWrapper\Responses\Users;

use AKJAbstract\APIWrapper\Objects\AffinityUserObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetUsersResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityUserObject;

    protected $affinity_user_objects = [];

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinity_user_objects = [];

        foreach($affinity_response->AffinityUsers->AffinityUser as $affinity_user){
            $affinity_user_object = new AffinityUserObject();

            $affinity_user_object->setObject($affinity_user);

            $this->affinity_user_objects[] = $affinity_user_object;
        }

        return $this;
    }

    public function getResponse(): array
    {
        return $this->affinity_user_objects;
    }
}