<?php namespace AKJAbstract\APIWrapper\Responses\Users;

use AKJAbstract\APIWrapper\Objects\AffinityTicketObject;
use AKJAbstract\APIWrapper\Objects\AffinityUserObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetUserResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityUserObject;

    public function __construct()
    {
        $this->affinityUserObject = new AffinityUserObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityUserObject->setObject($affinity_response->AffinityUser);

        return $this;
    }

    public function getResponse(): AffinityUserObject
    {
        return $this->affinityUserObject;
    }
}