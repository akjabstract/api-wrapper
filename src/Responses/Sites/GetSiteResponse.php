<?php namespace AKJAbstract\APIWrapper\Responses\Sites;

use AKJAbstract\APIWrapper\Objects\AffinitySiteObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetSiteResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinitySiteObject;

    public function __construct()
    {
        $this->affinitySiteObject = new AffinitySiteObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinitySiteObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinitySiteObject
    {
        return $this->affinitySiteObject;
    }
}