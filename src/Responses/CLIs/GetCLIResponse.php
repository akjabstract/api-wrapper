<?php namespace AKJAbstract\APIWrapper\Responses\CLIs;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetCLIResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityCLIObject;

    public function __construct()
    {
        $this->affinityCLIObject = new AffinityCLIObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityCLIObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityCLIObject
    {
        return $this->affinityCLIObject;
    }
}