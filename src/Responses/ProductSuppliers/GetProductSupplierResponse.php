<?php namespace AKJAbstract\APIWrapper\Responses\ProductSuppliers;

use AKJAbstract\APIWrapper\Objects\AffinityProductSupplierObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetProductSupplierResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityProductSupplierObject;

    public function __construct()
    {
        $this->affinityProductSupplierObject = new AffinityProductSupplierObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityProductSupplierObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityProductSupplierObject
    {
        return $this->affinityProductSupplierObject;
    }
}