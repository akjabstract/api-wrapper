<?php namespace AKJAbstract\APIWrapper\Responses\Tasks;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinityTaskObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetTaskResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityTaskObject;

    public function __construct()
    {
        $this->affinityTaskObject = new AffinityTaskObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityTaskObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityTaskObject
    {
        return $this->affinityTaskObject;
    }
}