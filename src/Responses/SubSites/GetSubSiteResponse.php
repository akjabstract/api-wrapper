<?php namespace AKJAbstract\APIWrapper\Responses\SubSites;

use AKJAbstract\APIWrapper\Objects\AffinityCLIObject;
use AKJAbstract\APIWrapper\Objects\AffinitySubSiteObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetSubSiteResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinitySubSitesObject;

    public function __construct()
    {
        $this->affinitySubSitesObject = new AffinitySubSiteObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinitySubSitesObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinitySubSiteObject
    {
        return $this->affinitySubSitesObject;
    }
}