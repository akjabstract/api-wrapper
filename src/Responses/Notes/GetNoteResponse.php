<?php namespace AKJAbstract\APIWrapper\Responses\Notes;

use AKJAbstract\APIWrapper\Objects\AffinityNoteObject;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\ResponseInterface;

class GetNoteResponse extends AbstractResponse implements ResponseInterface
{
    protected $affinityNoteObject;

    public function __construct()
    {
        $this->affinityNoteObject = new AffinityNoteObject();
    }

    public function setResponse(\stdClass $affinity_response)
    {
        $this->affinityNoteObject->setObject($affinity_response);

        return $this;
    }

    public function getResponse(): AffinityNoteObject
    {
        return $this->affinityNoteObject;
    }
}