<?php namespace AKJAbstract\APIWrapper\API;

class AffinitySalesLedgerBalancesAPI extends AffinityAPICore
{
    /**
     * Gets the tickets with a recent event
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @throws \Exception
     * @return object
     */
    protected function getSalesLedgerBalances(Filter $filter, $offset = 0, $qty = 30, Projection $projection = null, Ordering $ordering = null)
    {
        if(!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('SiteID', 'Desc');

        $adhocArguments = [
            'ObjectName' => 'SalesLedgerBalances',
            'XmlRootNode' => 'Balances',
            'ObjectIDField' => 'SiteID',
        ];

        $response = $this->client->XMLAdhocSalesLedgerBalancesView(array('identityToken' => $this->getToken(), 'adhocArguments' => $adhocArguments, 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML()));

        dd($response);

        if ($this->errorResponse($response->XmlQueryAdhocObjectsResult)) throw new \Exception('Affinity missing results');

        return $this->xmlToObject($response->XmlQueryAdhocObjectsResult->Result);
    }
}