<?php

namespace AKJAbstract\APIWrapper\API;

use AKJAbstract\APIWrapper\Responses\SiteProducts\GetSiteProductResponse;
use AKJAbstract\APIWrapper\Responses\SiteProducts\GetSiteProductsResponse;

/**
 * Class AffinitySiteBroadbandAPI
 * @package App\Packages\Affinity
 */
class AffinitySiteProductAPI extends AffinityAPICore
{
    protected $get_site_product_response;

    protected $get_site_products_response;

    public function __construct()
    {
        $this->get_site_product_response = new GetSiteProductResponse();

        $this->get_site_products_response = new GetSiteProductsResponse();

        parent::__construct();
    }

    /**
     * Get the SiteProduct record by its id number
     * @param int $site_product_id
     * @return object
     * @throws \Exception
     */
    protected function getSiteProductByID(int $site_product_id)
    {
        $response = $this->client->XmlGetSiteProductById(array('identityToken' => $this->getToken(), 'SiteProductID' => $site_product_id));

        if ($this->errorResponse($response->XmlGetSiteProductByIDResult)) throw new \Exception('Affinity missing results');

        return $this->xmlToObject($response->XmlGetSiteProductByIDResult->Result, false);
    }

    /**
     * Gets the number of site product based on the filter
     * @param Filter $filter
     * @throws \Exception
     * @return int
     */
    protected function getSiteProductItemCount(Filter $filter):int
    {

        $response = $this->client->XmlGetSiteProductItemCount(array('identityToken' => $this->getToken(), 'filterXml' => $filter->getFilterXML()));

        if ($this->errorResponse($response->XmlGetSiteProductItemCountResult)) throw new \Exception('API response had no results');

        $result = $this->xmlToObject($response->XmlGetSiteProductItemCountResult->Result);

        return $result->SiteProductItemCount;
    }

    /**
     * Find SiteBroadband records based on a XML filter query. Fields like SiteID, CliID, OrderReference, Username, Password,
     * SiteBroadbandID, SiteBroadbandUID
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @throws \Exception
     * @return object
     */
    protected function getSiteProducts(Filter $filter, $offset = 0, $qty = 30, ?Projection $projection = null, ?Ordering $ordering = null)
    {
        if (!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('SiteProductItemID', 'Asc');

        $response = $this->client->XmlQuerySiteProductItems(array('identityToken' => $this->getToken(), 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML()));

        if ($this->errorResponse($response->XmlQuerySiteProductItemsResult)) throw new \Exception('Affinity missing results');

        $result = $this->xmlToObject($response->XmlQuerySiteProductItemsResult->Result);

        $this->get_site_products_response->setResponse($result);

        return $this->get_site_products_response;
    }
}