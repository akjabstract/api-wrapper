<?php namespace AKJAbstract\APIWrapper\API;

use AKJAbstract\APIWrapper\Objects\AffinityAddressObject;
use AKJAbstract\APIWrapper\Responses\Addresses\GetAddressResponse;
use AKJAbstract\APIWrapper\Responses\CLIs\GetAddressesResponse;

/**
 * Class AffinityOrderAPI
 * @package App\Packages\Affinity
 */
class AffinityOrderAPI extends AffinityAPICore
{
    protected $get_address_response;

    protected $get_addresses_response;

    public function __construct()
    {
        //TODO: this is not finished at all, pending investigation
        $this->get_address_response = new GetAddressResponse();

        $this->get_addresses_response = new GetAddressesResponse();

        parent::__construct();
    }

    /**
     * Get order by ID
     * @param $order_id
     * @return AffinityAddressObject
     */
    protected function getOrderByID(int $order_id):?AffinityAddressObject
    {
        try {
            $response = $this->client->GetNewOrderByID([
                'identityToken' => $this->getToken(),
                'newOrderID' => $order_id
            ]);
            dd($response);
            if ($this->errorResponse($response->XmlGetAddressByConnectionResult)) throw new \Exception('No address result received.' . serialize($response));

            $result = $this->xmlToObject($response->XmlGetAddressByConnectionResult->Result);

            $this->get_address_response->setResponse($result->Address);

            return $this->get_address_response->getResponse();

        }catch (\Exception $e){

            var_dump($e->getMessage());

            dd($response);
        }

        return $response;

    }
}