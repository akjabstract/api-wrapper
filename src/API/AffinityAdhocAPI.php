<?php namespace AKJAbstract\APIWrapper\API;

use Carbon\Carbon;

class AffinityAdhocAPI extends AffinityAPICore
{
    /**
     * Gets the adhoc result
     * @param ArgumentsAdhoc $argumentsAdhoc
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @return object
     * @throws \Exception
     */
    protected function getAdhoc(ArgumentsAdhoc $argumentsAdhoc, Filter $filter, $offset = 0, $qty = 30, Projection $projection = null, Ordering $ordering = null)
    {
        if (!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering;

        $response = $this->client->XmlQueryAdhocObjects(array('identityToken' => $this->getToken(), 'adhocArguments' => $argumentsAdhoc, 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML()));

        if ($this->errorResponse($response->XmlQueryAdhocObjectsResult)) throw new \Exception('Affinity missing results');

        return $this->xmlToObject($response->XmlQueryAdhocObjectsResult->Result);
    }

    /**
     * Gets the adhoc count
     * @param ArgumentsAdhoc $argumentsAdhoc
     * @param Filter $filter
     * @return int
     * @throws \Exception
     */
    protected function getAdhocCount(ArgumentsAdhoc $argumentsAdhoc, Filter $filter)
    {
        $response = $this->client->XmlGetAdhocObjectCount(array('identityToken' => $this->getToken(), 'adhocArguments' => $argumentsAdhoc, 'filterXml' => $filter->getFilterXML()));

        if ($this->errorResponse($response->XmlGetAdhocObjectCountResult)) throw new \Exception('Affinity missing results');

        $result = $this->xmlToObject($response->XmlGetAdhocObjectCountResult->Result);

        $count_field = $argumentsAdhoc->ObjectName . 'Count';

        return (int)$result->$count_field;
    }

    /**
     * Gets the tickets with a recent event
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @return object
     * @throws \Exception
     */
    protected function getAdhocTickets(Filter $filter, $offset = 0, $qty = 30, Projection $projection = null, Ordering $ordering = null)
    {
        if (!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('Date', 'Desc');

        $adhocArguments = [
            'ObjectName' => 'TicketEvent',
            'XmlRootNode' => 'TicketID',
            'ObjectIDField' => 'Events',
        ];

        $response = $this->client->XmlQueryAdhocObjects(array('identityToken' => $this->getToken(), 'adhocArguments' => $adhocArguments, 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML()));

        if ($this->errorResponse($response->XmlQueryAdhocObjectsResult)) throw new \Exception('Affinity missing results');

        return $this->xmlToObject($response->XmlQueryAdhocObjectsResult->Result);
    }

    /**
     * Gets the quantity of recent event tickets
     * @param Filter $filter
     * @return int
     * @throws \Exception
     */
    protected function getAdhocTicketsCount(Filter $filter)
    {
        $adhocArguments = [
            'ObjectName' => 'TicketEvent',
            'XmlRootNode' => 'TicketID',
            'ObjectIDField' => 'Events',
        ];

        $response = $this->client->XmlGetAdhocObjectCount(array('identityToken' => $this->getToken(), 'adhocArguments' => $adhocArguments, 'filterXml' => $filter->getFilterXML()));

        if ($this->errorResponse($response->XmlGetAdhocObjectCountResult)) throw new \Exception('Affinity missing results');

        $result = $this->xmlToObject($response->XmlGetAdhocObjectCountResult->Result);

        return (int)$result->TicketEventCount;
    }
}