<?php namespace AKJAbstract\APIWrapper\API;

use AKJAbstract\APIWrapper\Requests\SubSites\SubSiteUpdateRequest;
use AKJAbstract\APIWrapper\Responses\SubSites\GetSubSiteResponse;
use AKJAbstract\APIWrapper\Responses\SubSites\GetSubSitesResponse;

/**
 * Class AffinitySiteAPI
 * @package App\Packages\Affinity
 */
class AffinitySubSiteAPI extends AffinityAPICore
{
    protected $get_subsite_response;

    protected $get_subsites_response;

    public function __construct()
    {
        $this->get_subsite_response = new GetSubSiteResponse();

        $this->get_subsites_response = new GetSubSitesResponse();

        parent::__construct();
    }

    /**
     * Gets the number of sites based on the filter
     * @param Filter $filter
     * @return int
     */
    protected function getSubSiteCount(Filter $filter): int
    {
        $response = $this->client->XmlGetSubSiteCount(array('identityToken' => $this->getToken(), 'filterXml' => $filter->getFilterXML()));

        if ($this->errorResponse($response->XmlGetSubSiteCountResult)) throw new \Exception('No subsite count');

        $result = $this->xmlToObject($response->XmlGetSubSiteCountResult->Result);

        return $result->SubSiteCount;
    }

    /**
     * Gets the Sub Site information based on site ID
     * @param int $sub_site_id
     * @return GetSubSitesResponse
     * @throws \Exception
     */
    protected function getSubSiteByID(int $sub_site_id): ?GetSubSiteResponse
    {
        $response = $this->client->XmlGetSubSiteById(array('identityToken' => $this->getToken(), 'subSiteID' => $sub_site_id));

        if ($this->errorResponse($response->XmlGetSubSiteByIDResult)) throw new \Exception('Affinity missing results');

        $result = $this->xmlToObject($response->XmlGetSubSiteByIDResult->Result);

        $this->get_subsite_response->setResponse($result->SubSite);

        return $this->get_subsite_response;
    }


    /**
     * Gets the Sub Site Address based on SubSiteID
     * @param $sub_site_id
     * @return object
     */
    protected function GetSubSiteAddressByID(int $sub_site_id): \stdClass
    {
        try {
            $response = $this->client->GetSubSiteAddressByID(new \SoapVar('<ns1:GetSubSiteAddressByID>', XSD_ANYXML), new \SoapVar('<ns1:IdentityToken>' . $this->getToken() . '</ns1:IdentityToken>', XSD_ANYXML), new \SoapVar('<ns1:subSiteID>' . $sub_site_id . '</ns1:subSiteID>', XSD_ANYXML), new \SoapVar('</ns1:GetSubSiteAddressByID>', XSD_ANYXML));

            return $response;
        } catch (\Exception $e) {
            dd($e);
        }
    }


    /**
     * Gets the Sub Site ID based on ref code
     * @param string $sub_site_ref
     * @return object
     */
    protected function getSubSiteByRef(string $sub_site_ref): int
    {
        try {
            $response = $this->client->GetSubSiteIDByRef(new \SoapVar('<ns1:GetSiteIDByRef>', XSD_ANYXML), new \SoapVar('<ns1:IdentityToken>' . $this->getToken() . '</ns1:IdentityToken>', XSD_ANYXML), new \SoapVar('<ns1:siteRef>' . $sub_site_ref . '</ns1:siteRef>', XSD_ANYXML), new \SoapVar('</ns1:GetSiteIDByRef>', XSD_ANYXML));
        } catch (\Exception $e) {
            dd($e);
        }

        return $response->subSiteID;
    }

    /**
     * Gets a list of Sub Sites based on filter
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @return GetSubSitesResponse
     * @throws \Exception
     */
    protected function getSubSites(Filter $filter, $offset = 0, $qty = 30, ?Projection $projection = null, ?Ordering $ordering = null): ?GetSubSitesResponse
    {
        if (!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('SubSiteID', 'Asc');

        $response = $this->client->XmlQuerySubSites(array('identityToken' => $this->getToken(), 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML()));

        if ($this->errorResponse($response->XmlQuerySubSitesResult)) throw new \Exception('No subsite result');

        $result = $this->xmlToObject($response->XmlQuerySubSitesResult->Result);

        $this->get_subsites_response->setResponse($result);

        return $this->get_subsites_response;
    }

    /**
     * Creats a new subsite and returns subsite ID
     * @param string $sub_site_name
     * @param string $sub_site_ref
     * @param int $site_id
     * @return int siteID
     * @throws \Exception
     */
    protected function createNewSubSite(string $sub_site_name, string $sub_site_ref, int $site_id): ?int
    {
        $response = $this->client->CreateNewSubSiteByID(new \SoapVar('<ns1:CreateNewSubSiteByID>', XSD_ANYXML),
            new \SoapVar('<ns1:IdentityToken>' . $this->getToken() . '</ns1:IdentityToken>', XSD_ANYXML),
            new \SoapVar('<ns1:siteID>' . $site_id . '</ns1:siteID>', XSD_ANYXML),
            new \SoapVar('<ns1:subSiteName>' . $sub_site_name . '</ns1:subSiteName>', XSD_ANYXML),
            new \SoapVar('<ns1:subSiteRef>' . $sub_site_ref . '</ns1:subSiteRef>', XSD_ANYXML),
            new \SoapVar('</ns1:CreateNewSubSiteByID>', XSD_ANYXML));

        if ($this->errorResponse($response->CreateNewSubSiteByIDResult)) throw new \Exception('Affinity missing results');

        return $response->subSiteID;
    }

    /**
     * Updates a site using the site ID
     * @param int $sub_site_id
     * @param SubSiteUpdateRequest $subSiteUpdateRequest
     * @return object
     * @throws \Exception
     */
    protected function updateSubSiteByID(int $sub_site_id, SubSiteUpdateRequest $subSiteUpdateRequest)
    {
        if (!$subSiteUpdateRequest->isValid()) throw new \Exception('Update sub-site request not valid. ' . serialize($subSiteUpdateRequest->getErrors()));

        $sub_site_xml = $this->arrayToXMLString($subSiteUpdateRequest->getRequestArray(), 'SubSite');

        $response = $this->client->XmlUpdateSubSiteByID(array('identityToken' => $this->getToken(), 'subSiteID' => $sub_site_id, 'subSiteXml' => $sub_site_xml));

        if ($this->errorResponse($response->XmlUpdateSubSiteByIDResult)) throw new \Exception('Affinity missing results');

        return $this->xmlToObject($response->XmlUpdateSubSiteByIDResult->Result);
    }

    /**
     * Updates a subsite address
     * @param int $sub_site_id
     * @param string $address_1
     * @param string $address_2
     * @param string $address_3
     * @param string $address_4
     * @param string $address_5
     * @param string $address_6
     * @return mixed
     * @throws \Exception
     */
    protected function updateSubSiteAddress(int $sub_site_id, string $address_1, string $address_2, ?string $address_3, ?string $address_4, ?string $address_5, ?string $address_6)
    {
        $response = $this->client->UpdateSubSiteAddressByID(new \SoapVar('<ns1:UpdateSubSiteAddressByID>', XSD_ANYXML),
            new \SoapVar('<ns1:IdentityToken>' . $this->getToken() . '</ns1:IdentityToken>', XSD_ANYXML),
            new \SoapVar('<ns1:subSiteID>' . $sub_site_id . '</ns1:subSiteID>', XSD_ANYXML),
            new \SoapVar('<ns1:address1>' . $address_1 . '</ns1:address1>', XSD_ANYXML),
            new \SoapVar('<ns1:address2>' . $address_2 . '</ns1:address2>', XSD_ANYXML),
            new \SoapVar('<ns1:address3>' . $address_3 . '</ns1:address3>', XSD_ANYXML),
            new \SoapVar('<ns1:address4>' . $address_4 . '</ns1:address4>', XSD_ANYXML),
            new \SoapVar('<ns1:address5>' . $address_5 . '</ns1:address5>', XSD_ANYXML),
            new \SoapVar('<ns1:address6>' . $address_6 . '</ns1:address6>', XSD_ANYXML),
            new \SoapVar('</ns1:UpdateSubSiteAddressByID>', XSD_ANYXML));

        if ($this->errorResponse($response->UpdateSubSiteAddressByIDResult)) throw new \Exception('Affinity missing results');

        return $response->UpdateSubSiteAddressByIDResult;
    }

}