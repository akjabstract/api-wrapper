<?php

namespace AKJAbstract\APIWrapper\API;

use AKJAbstract\APIWrapper\Responses\Sites\GetSiteBroadbandResponse;
use AKJAbstract\APIWrapper\Responses\Sites\GetSiteBroadbandsResponse;

/**
 * Class AffinitySiteBroadbandAPI
 * @package App\Packages\Affinity
 */
class AffinitySiteBroadbandAPI extends AffinityAPICore
{
    protected $get_site_broadband_response;

    protected $get_site_broadbands_response;

    public function __construct()
    {
        $this->get_site_broadband_response = new GetSiteBroadbandResponse();

        $this->get_site_broadbands_response = new GetSiteBroadbandsResponse();

        parent::__construct();
    }

    /**
     * Get the SiteBroadband record by its id number
     * @param int $site_broadband_id
     * @throws \Exception
     * @return object
     */
    protected function getSiteBroadbandByID(int $site_broadband_id)
    {
        $response = $this->client->XmlGetSiteBroadbandById(array('identityToken' => $this->getToken(), 'SiteBroadbandID' => $site_broadband_id));

        if ($this->errorResponse($response->XmlGetSiteBroadbandByIDResult)) throw new \Exception('Affinity missing results');

        return $this->xmlToObject($response->XmlGetSiteBroadbandByIDResult->Result, false);
    }

    /**
     * Gets the number of site broadband based on the filter
     * @param Filter $filter
     * @throws \Exception
     * @return int
     */
    protected function getSiteBroadbandCount(Filter $filter):int
    {
        $response = $this->client->XmlGetSiteBroadbandCount(array('identityToken' => $this->getToken(), 'filterXml' => $filter->getFilterXML()));

        if ($this->errorResponse($response->XmlGetSiteBroadbandResult)) throw new \Exception('API response had no results');

        $result = $this->xmlToObject($response->XmlGetSiteBroadbandResult->Result);

        return $result->SiteBroadbandCount;
    }

    /**
     * Find SiteBroadband records based on a XML filter query. Fields like SiteID, CliID, OrderReference, Username, Password,
     * SiteBroadbandID, SiteBroadbandUID
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @throws \Exception
     * @return object
     */
    protected function getSiteBroadbands(Filter $filter, $offset = 0, $qty = 30, ?Projection $projection = null, ?Ordering $ordering = null)
    {
        if (!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('SiteBroadbandID', 'Asc');

        $response = $this->client->XmlQuerySiteBroadband(array('identityToken' => $this->getToken(), 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML()));

        if ($this->errorResponse($response->XmlQuerySiteBroadbandResult)) throw new \Exception('Affinity missing results');

        $result = $this->xmlToObject($response->XmlQuerySiteBroadbandResult->Result);

        $this->get_site_broadbands_response->setResponse($result);

        return $this->get_site_broadbands_response;
    }
}