<?php namespace AKJAbstract\APIWrapper\API;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Responses\Tariffs\GetAssignedTariffResponse;
use AKJAbstract\APIWrapper\Responses\Tariffs\GetAssignedTariffsResponse;
use AKJAbstract\APIWrapper\Responses\Tariffs\GetTariffResponse;
use AKJAbstract\APIWrapper\Responses\Tariffs\GetTariffsResponse;

/**
 * Class AffinityTSettings
 * @package App\Packages\Affinity
 */
class AffinitySettings extends AffinityAPICore
{
    public function __construct()
    {
        parent::__construct();
    }
}