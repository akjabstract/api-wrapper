<?php namespace AKJAbstract\APIWrapper\API;

use AKJAbstract\APIWrapper\Objects\AffinityAddressObject;
use AKJAbstract\APIWrapper\Requests\Addresses\AddressUpdateRequest;
use AKJAbstract\APIWrapper\Responses\Addresses\GetAddressResponse;
use AKJAbstract\APIWrapper\Responses\CLIs\GetAddressesResponse;

/**
 * Class AffinityAddressAPI
 * @package App\Packages\Affinity
 */
class AffinityAddressAPI extends AffinityAPICore
{
    protected $get_address_response;

    protected $get_addresses_response;

    public function __construct()
    {
        $this->get_address_response = new GetAddressResponse();

        $this->get_addresses_response = new GetAddressesResponse();

        parent::__construct();
    }

    /**
     * Get address by SiteUID
     * @param $site_uid
     * @param string $address_type - ‘Company’, ‘Site’, ‘Billing’, ‘Bank’
     * @return AffinityAddressObject
     */
    protected function getAddressBySiteUID(string $site_uid, string $address_type = 'Site'): ?AffinityAddressObject

    {
        $connection = [
            'ConnectionType' => 'Site',
            'ConnectionKeyType' => 'UniqueIdentifier',
            'ConnectionKeyReference' => $site_uid
        ];

        try {
            $response = $this->client->XmlGetAddressByConnection([
                'identityToken' => $this->getToken(),
                'connection' => $connection,
                'addressType' => $address_type
            ]);

            if ($this->errorResponse($response->XmlGetAddressByConnectionResult)) throw new \Exception('No address result received.' . serialize($response));

            $result = $this->xmlToObject($response->XmlGetAddressByConnectionResult->Result);

            $this->get_address_response->setResponse($result->Address);

            return $this->get_address_response->getResponse();

        } catch (\Exception $e) {

            return null;
        }
    }

    /**
     * updates Address by site ID
     * @param AddressUpdateRequest $addressUpdateRequest
     * @param string $connection_type
     * @return object
     */
    protected function updateAddressBySite(AddressUpdateRequest $addressUpdateRequest, string $connection_type = 'Site')
    {
        $address_array = [
            'address1' => $addressUpdateRequest->getField('Address1'),
            'address2' => $addressUpdateRequest->getField('Address2'),
            'address3' => $addressUpdateRequest->getField('Address3'),
            'address4' => $addressUpdateRequest->getField('Town'),
            'address5' => $addressUpdateRequest->getField('County'),
            'address6' => $addressUpdateRequest->getField('PostCode'),
        ];

        $address_xml = $this->arrayToXMLString($address_array, 'Address');

        try {
            $response = $this->client->XmlUpdateAddressByConnection(array('identityToken' => $this->getToken(),
                'connection' => array(
                    'ConnectionType' => $connection_type,
                    'ConnectionKeyType' => 'ID',
                    'ConnectionKeyReference' => $addressUpdateRequest->getField('SiteID')
                ),
                'addressType' => $addressUpdateRequest->getField('AddressType'),
                'addressXml' => $address_xml
            ));

            if ($this->errorResponse($response->XmlUpdateAddressByConnectionResult)) throw new \Exception('Affinity missing results');

            return $this->xmlToObject($response->XmlUpdateAddressByConnectionResult->Result);
        } catch (\Exception $e) {

            return null;
        }
    }
}