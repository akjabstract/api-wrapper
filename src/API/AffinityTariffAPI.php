<?php namespace AKJAbstract\APIWrapper\API;

use AKJAbstract\APIWrapper\Responses\Tariffs\GetAssignedDealerTariffResponse;
use Carbon\Carbon;
use AKJAbstract\APIWrapper\Responses\Tariffs\GetAssignedTariffResponse;
use AKJAbstract\APIWrapper\Responses\Tariffs\GetAssignedTariffsResponse;
use AKJAbstract\APIWrapper\Responses\Tariffs\GetTariffResponse;
use AKJAbstract\APIWrapper\Responses\Tariffs\GetTariffsResponse;
use AKJAbstract\APIWrapper\Responses\Tariffs\GetDealersTariffsResponse;

/**
 * Class AffinityTariffAPI
 * @package App\Packages\Affinity
 */
class AffinityTariffAPI extends AffinityAPICore
{
    protected $get_tariff_response;

    protected $get_tariffs_response;

    protected $get_dealers_tariffs_response;

    protected $get_assigned_tariff_response;

    protected $get_assigned_tariffs_response;

    protected $get_assigned_dealer_tariff_response;

    public function __construct()
    {
        $this->get_tariff_response = new GetTariffResponse();

        $this->get_tariffs_response = new GetTariffsResponse();

        $this->get_dealers_tariffs_response = new GetDealersTariffsResponse();

        $this->get_assigned_tariff_response = new GetAssignedTariffResponse();

        $this->get_assigned_tariffs_response = new GetAssignedTariffsResponse();

        $this->get_assigned_dealer_tariff_response = new GetAssignedDealerTariffResponse();

        parent::__construct();
    }

    protected function getProductTariffBySiteID($site_id)
    {
        $response = $this->client->XmlGetAssignedProductTariffSchemeByID(array('identityToken' => $this->getToken(), 'assignedID' => $site_id, 'connectionType' => 'Site'));

        if ($this->errorResponse($response->XmlGetAssignedProductTariffSchemeByIDResult)) throw new \Exception('Affinity missing results ' . serialize($response));

        return $this->xmlToObject($response->XmlGetAssignedProductTariffSchemeByIDResult->Result);
    }

    /**
     * Get the product tariffs
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @throws \Exception
     * @return object
     */
    protected function getProductTariffs(Filter $filter, $offset = 0, $qty = 30, ?Projection $projection = null, ?Ordering $ordering = null)
    {
        if(!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('SchemeID', 'Asc');

        $response = $this->client->XmlQueryProductTariffSchemes(array('identityToken' => $this->getToken(), 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML()));

        if($this->errorResponse($response->XmlQueryProductTariffSchemesResult)) throw new \Exception('No tariff response ' . serialize($response));

        $result = $this->xmlToObject($response->XmlQueryProductTariffSchemesResult->Result);

        $this->get_tariffs_response->setResponse($result);

        return $this->get_tariffs_response;
    }

    /**
     * Get the dealer tariffs
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @throws \Exception
     * @return object
     */
    protected function getDealerTariffs(Filter $filter, $offset = 0, $qty = 30, ?Projection $projection = null, ?Ordering $ordering = null)
    {
        if(!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('SchemeID', 'Asc');

        $response = $this->client->XmlQueryDealerTariffSchemes(array('identityToken' => $this->getToken(), 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML()));

        if($this->errorResponse($response->XmlQueryDealerTariffSchemesResult)) throw new \Exception('No dealer tariff response ' . serialize($response));

        $result = $this->xmlToObject($response->XmlQueryDealerTariffSchemesResult->Result);

        $this->get_dealers_tariffs_response->setResponse($result);

        return $this->get_dealers_tariffs_response;
    }

    /**
     * Get the product assigned tariffs, maybe to Site
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @throws \Exception
     * @return GetAssignedTariffsResponse
     */
    protected function getAssignedProductTariffs(Filter $filter, $offset = 0, $qty = 30, ?Projection $projection = null, ?Ordering $ordering = null):?GetAssignedTariffsResponse
    {
        if(!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('SiteID', 'Asc');

        $response = $this->client->XmlQueryAssignedProductTariffSchemes(array('identityToken' => $this->getToken(), 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML(), 'connectionType' => 'Site'));

        if($this->errorResponse($response->XmlQueryAssignedProductTariffSchemesResult)) throw new \Exception('No assigned tariff response ' . serialize($response));

        $result = $this->xmlToObject($response->XmlQueryAssignedProductTariffSchemesResult->Result);

        $this->get_assigned_tariffs_response->setResponse($result);

        return $this->get_assigned_tariffs_response;
    }

    /**
     * Get the product assigned tariffs, maybe to Site
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @throws \Exception
     * @return int
     */
    protected function getAssignedProductTariffsCount(Filter $filter, $offset = 0, $qty = 30, ?Projection $projection = null, ?Ordering $ordering = null):int
    {
        if(!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('SiteID', 'Asc');

        $response = $this->client->XmlGetAssignedProductTariffSchemeCount(array('identityToken' => $this->getToken(), 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML(), 'connectionType' => 'Site'));

        if($this->errorResponse($response->XmlGetAssignedProductTariffSchemeCountResult)) throw new \Exception('No tariff response ' . serialize($response));

        $result = $this->xmlToObject($response->XmlGetAssignedProductTariffSchemeCountResult->Result);

        $this->get_tariffs_response->setResponse($result);

        return (int)$result->SiteProductTariffSchemeCount;
    }

    /**
     * Assigns a product tariff to a site
     * @param $assign_array ['SiteID', 'SchemeID', 'StartDate']
     * @throws \Exception
     * @return GetAssignedTariffResponse|false
     */
    protected function assignProductTariff($assign_array):?GetAssignedTariffResponse
    {
        $assignedTariffSchemeXml = $this->arrayToXMLString($assign_array, 'SiteProductTariffScheme');

        $response = $this->client->XmlAddAssignedProductTariffScheme(array('identityToken' => $this->getToken(), 'connectionType' => 'Site', 'assignedTariffSchemeXml' => $assignedTariffSchemeXml));

        if ($this->errorResponse($response->XmlAddAssignedProductTariffSchemeResult)) throw new \Exception('No tickets result received.' . serialize($response));

        $result = $this->xmlToObject($response->XmlAddAssignedProductTariffSchemeResult->Result);

        $this->get_assigned_tariff_response->setResponse($result->SiteProductTariffScheme);

        return $this->get_assigned_tariff_response;
    }

    /**
     * @param $assign_array ['SchemeID', 'Service', 'StartDate', 'EndDate', 'CRMUserID', 'CompanyID']
     * @return GetAssignedDealerTariffResponse|null
     * @throws \Exception
     */
    protected function assignDealerTariff($assign_array):?GetAssignedDealerTariffResponse
    {
        $assignedTariffSchemeXml = $this->arrayToXMLString($assign_array, 'CompanyCallTariffScheme');

        $response = $this->client->XmlAddAssignedCallTariffScheme(array('identityToken' => $this->getToken(), 'connectionType' => 'Company', 'assignedTariffSchemeXml' => $assignedTariffSchemeXml));

        if ($this->errorResponse($response->XmlAddAssignedCallTariffSchemeResult)) throw new \Exception('No tickets result received.' . serialize($response));

        $cleanedResult = str_replace('<?xml version="1.0" encoding="utf-16"?>', '', $response->XmlAddAssignedCallTariffSchemeResult->Result);

        $result = $this->xmlToObject($cleanedResult);

        $this->get_assigned_dealer_tariff_response->setResponse($result->CompanyCallTariffScheme);

        return $this->get_assigned_dealer_tariff_response;
    }


    protected function getSiteTariffOverride($product_item_id, Carbon $date)
    {
        $response = $this->client->XmlGetSiteProductItemTariffOverrideByIDAndDate(array('identityToken' => $this->getToken(), 'siteProductItemID' => $product_item_id, 'tariffDate' => $date->toW3cString()));

        dd($response);
    }

    protected function addSiteTariffOverride($site_product_item_id, $cost_in_pence, Carbon $date)
    {
        $tariff_override = [
            'SiteProductItemID' => $site_product_item_id,
            'StartDate' => $date->startOfDay()->toW3cString(),
            'TariffLevel' => 'Site',
            'CurrencyCode' => null,
            'TariffParts' => [
                'TariffPart' => [
                    'PartOrder' => 0,
                    'MaxDurationInMonths' => 999,
                    'Cost' => $cost_in_pence / 100,
                    'Uplift' => 0,
                ]
            ]
        ];

        $tariffOverrideXml = $this->arrayToXMLString($tariff_override, 'SiteProductItemTariffOverride');

        return $this->client->XmlAddSiteProductItemTariffOverride(array('identityToken' => $this->getToken(), 'tariffOverrideXml' => $tariffOverrideXml));
    }
}