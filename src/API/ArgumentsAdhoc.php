<?php namespace AKJAbstract\APIWrapper\API;

/**
 * Class ArgumentsAdhoc
 * @package App\Packages\Affinity
 */
class ArgumentsAdhoc
{
    public $ObjectName;

    public $XmlRootNode;

    public $ObjectIDField;

    public function setObjectName($object_name)
    {
        $this->ObjectName = $object_name;

        return $this;
    }

    public function setXmlRootNode($xml_root_node)
    {
        $this->XmlRootNode = $xml_root_node;

        return $this;
    }

    public function setObjectIDField($object_id_field)
    {
        $this->ObjectIDField = $object_id_field;

        return $this;
    }

    public function getAhdhocArguments()
    {
        return  [
            'ObjectName' => $this->ObjectName,
            'XmlRootNode' => $this->XmlRootNode,
            'ObjectIDField' => $this->ObjectIDField,
        ];
    }
}