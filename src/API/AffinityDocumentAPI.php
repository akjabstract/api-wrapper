<?php namespace AKJAbstract\APIWrapper\API;

/**
 * Class AffinityTaskAPI
 * @package App\Packages\Affinity
 */
class AffinityDocumentAPI extends AffinityAPICore
{

    private $ns = 'http://tempuri.org/';
    private $tickedId;
    private $summary;
    private $category;
    private $subCategory;
    private $fileName;

    /**
     * @param $ticket_id
     * @return $this
     */
    private function ticketId($ticket_id){
        $this->tickedId = $ticket_id;
        return $this;
    }

    /**
     * @param $summary
     * @return $this
     */
    private function summary($summary){
        $this->summary = $summary;
        return $this;
    }

    /**
     * @param $category
     * @return $this
     */
    private function category($category){
        $this->category = $category;
        return $this;
    }

    /**
     * @param $subCategory
     * @return $this
     */
    private function subCategory($subCategory){
        $this->subCategory = $subCategory;
        return $this;
    }

    /**
     * @param $fileName
     * @return $this
     */
    public function fileName($fileName){
        $this->fileName = $fileName;
        return $this;
    }

    private function setHeaders($file){

        $headers[] = new \SoapHeader($this->ns,'UserName',$this->getUserName());
        $headers[] = new \SoapHeader($this->ns,'Summary',$this->summary);
        $headers[] = new \SoapHeader($this->ns,'Category',$this->category);
        $headers[] = new \SoapHeader($this->ns,'SubCategory',$this->subCategory);
        $headers[] = new \SoapHeader($this->ns,'Length',strlen($file));
        $headers[] = new \SoapHeader($this->ns,'IdentityToken',$this->getToken());
        $headers[] = new \SoapHeader($this->ns,'FileName',$this->fileName);
        $headers[] = new \SoapHeader(
            $this->ns,
            'Connection',
            array(
                'ConnectionType' => 'Ticket',
                'ConnectionKeyType' => 'ID',
                'ConnectionKeyReference' => $this->tickedId
            )
        );

        $this->client->__setSoapHeaders($headers);
    }

    protected function addDocumentToTicket($ticket_id, $file_location, $summary, $category, $subCategory)
    {

        $this->ticketID($ticket_id)
            ->summary($summary)
            ->category($category)
            ->subCategory($subCategory)
            ->fileName(basename($file_location));

        $file = file_get_contents($file_location);

        $this->setHeaders($file);

        $response = $this->client->CreateNewDocument(array(
            'FileByteStream' => $file
        ));

        return $response;
    }

    protected function addDocumentStreamToTicket($ticket_id, $file, $summary, $category, $subCategory, $fileName)
    {

        $this->ticketID($ticket_id)
            ->summary($summary)
            ->category($category)
            ->subCategory($subCategory)
            ->fileName($fileName);

        $this->setHeaders($file);

        $response = $this->client->CreateNewDocument(array(
            'FileByteStream' => $file
        ));

        return $response;
    }

    protected function addDocumentLinkToTask(int $task_id, string $category, string $subCategory, string $summary, string $userName, string $documentLink){

        $response = $this->client->CreateNewDocumentLink(array(
            'IdentityToken'  => $this->getToken(),
            'Category'       => $category,
            'subCategory'    => $subCategory,
            'summary'        => $summary,
            'userName'       => $userName,
            'documentLink'   => $documentLink,
            'connection' => array(
                'ConnectionType' => 'Task',
                'ConnectionKeyType' => 'ID',
                'ConnectionKeyReference' => $task_id
            )
        ));

        return $response;
    }

    protected function AddDocument()
    {
    }

    /**
     * add document to site
     * @param $task_id
     * @return object
     */
    protected function addDocumentToSite($site_id)
    {
    }

}