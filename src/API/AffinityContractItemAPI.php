<?php namespace AKJAbstract\APIWrapper\API;
use AKJAbstract\APIWrapper\Responses\Contracts\GetContractItemsResponse;

/**
 * Class AffinitySiteAPI
 * @package App\Packages\Affinity
 */
class AffinityContractItemAPI extends AffinityAPICore
{
    protected $get_contract_item_response;

    protected $get_contract_items_response;

    public function __construct()
    {
        $this->get_contract_item_response = new GetContractItemsResponse();
        $this->get_contract_items_response = new GetContractItemsResponse();

        parent::__construct();
    }

    /**
     * Gets a list of Sites based on filter
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @throws \Exception
     * @return GetContractItemsResponse
     */
    protected function getContractItems(Filter $filter, $offset = 0, $qty = 30, Projection $projection = null, Ordering $ordering = null):?GetContractItemsResponse
    {
        if(!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('ContractID', 'Asc');

        $response = $this->client->XmlQueryContractItem(array('identityToken' => $this->getToken(), 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML()));

        if ($this->errorResponse($response->XmlQueryContractItemResult)) throw new \Exception('Affinity missing results');

        $result = $this->xmlToObject($response->XmlQueryContractItemResult->Result);

        $this->get_contract_items_response->setResponse($result);

        return $this->get_contract_items_response;
    }

    /**
     * Gets the number of users based on the filter
     * @param Filter $filter
     * @throws \Exception
     * @return int
     */
    protected function getContractItemsCount(Filter $filter):int
    {
        $response = $this->client->XmlGetContractItemCount(array('identityToken' => $this->getToken(), 'filterXml' => $filter->getFilterXML()));

        if ($this->errorResponse($response->XmlGetContractItemCountResult)) throw new \Exception('Affinity missing results');

        $result = $this->xmlToObject($response->XmlGetContractItemCountResult->Result);

        return $result->ContractItemCount;
    }

}