<?php namespace AKJAbstract\APIWrapper\API;
use AKJAbstract\APIWrapper\Requests\Contracts\ContractCreateRequest;
use AKJAbstract\APIWrapper\Requests\Contracts\ContractUpdateRequest;
use AKJAbstract\APIWrapper\Responses\Contracts\GetContractsResponse;
use AKJAbstract\APIWrapper\Responses\Contracts\GetContractResponse;

/**
 * Class AffinitySiteAPI
 * @package App\Packages\Affinity
 */
class AffinityContractAPI extends AffinityAPICore
{
    protected $get_contract_response;

    protected $get_contracts_response;

    public function __construct()
    {
        $this->get_contract_response = new GetContractResponse();

        $this->get_contracts_response = new GetContractsResponse();

        parent::__construct();
    }

    /**
     * Gets a list of Sites based on filter
     * @param Filter $filter
     * @param int $offset
     * @param int $qty
     * @param Projection $projection
     * @param Ordering $ordering
     * @throws \Exception
     * @return GetContactsResponse
     */
    protected function getContracts(Filter $filter, $offset = 0, $qty = 30, Projection $projection = null, Ordering $ordering = null):?GetContractsResponse
    {
        if(!$projection) $projection = $this->default_projection;

        if (!$ordering) $ordering = $this->default_ordering->create()->field('ContractID', 'Asc');

        $response = $this->client->XmlQueryContract(array('identityToken' => $this->getToken(), 'numberOfItemsToSkip' => $offset, 'numberOfItemsToTake' => $qty, 'filterXml' => $filter->getFilterXML(), 'projectionXml' => $projection->getProjectionXML(), 'orderingXml' => $ordering->getOrderingXML()));

        if ($this->errorResponse($response->XmlQueryContractResult)) throw new \Exception('Affinity missing results');

        $result = $this->xmlToObject($response->XmlQueryContractResult->Result);

        $this->get_contracts_response->setResponse($result);

        return $this->get_contracts_response;
    }


    /**
     * Gets the number of users based on the filter
     * @param Filter $filter
     * @throws \Exception
     * @return int
     */
    protected function getContractsCount(Filter $filter):int
    {
        $response = $this->client->XmlGetContractCount(array('identityToken' => $this->getToken(), 'filterXml' => $filter->getFilterXML()));

        if ($this->errorResponse($response->XmlGetContractCountResult)) throw new \Exception('Affinity missing results');

        $result = $this->xmlToObject($response->XmlGetContractCountResult->Result);

        return $result->ContractCount;
    }


    /**
     * Create a contact
     * @param ContractCreateRequest $contactCreateRequest
     */
    protected function createContract(ContractCreateRequest $contractCreateRequest)
    {

        if (!$contractCreateRequest->isValid()) throw new \Exception('Create Contract request not valid. ' . serialize($contractCreateRequest->getErrors()));

        $contract_xml = $this->arrayToXMLString($contractCreateRequest->getRequestArray(), 'Contract');

        $response = $this->client->XmlAddContract(array('identityToken' => $this->getToken(), 'contractXml' => $contract_xml));

        if ($this->errorResponse($response->XmlAddContractResult)) throw new \Exception('No Contract create result received.' . serialize($response));

        $result = $this->xmlToObject($response->XmlAddContractResult->Result);

        $this->get_contract_response->setResponse($result->Contract);

        return $this->get_contract_response;
    }




    /**
     * @param int $contract_id
     * @param ContractUpdateRequest $contractUpdateRequest
     * @return GetContractResponse|null
     * @throws \Exception
     */
    protected function updateContractByID(int $contract_id, ContractUpdateRequest $contractUpdateRequest):?GetContractResponse
    {
        if (!$contractUpdateRequest->isValid()) throw new \Exception('Update Contract request not valid. ' . serialize($contractUpdateRequest->getErrors()));

        $contract_xml = $this->arrayToXMLString($contractUpdateRequest->getRequestArray(), 'Contract');

        $response = $this->client->XmlUpdateContractByID(array('identityToken' => $this->getToken(), 'contractID' => $contract_id, 'contractXml' => $contract_xml));

        if ($this->errorResponse($response->XmlUpdateContractByIDResult)) throw new \Exception('No contract update result received.' . serialize($response));

        $result = $this->xmlToObject($response->XmlUpdateContractByIDResult->Result);

        $this->get_contract_response->setResponse($result->Contract);

        return $this->get_contract_response;
    }




}