<?php namespace AKJAbstract\APIWrapper\API;

use AKJAbstract\APIWrapper\Requests\Associations\AssociationCreateRequest;
use AKJAbstract\APIWrapper\Responses\Associations\GetAssociationResponse;

/**
 * Class AffinityAssociationAPI
 * @package App\Packages\Affinity
 */
class AffinityAssociationsAPI extends AffinityAPICore
{

    protected $get_association_response;

    public function __construct()
    {
        $this->get_association_response = new GetAssociationResponse();

        parent::__construct();
    }

    /**
     * Creates an association record - needed for sub tickets etc.
     * @param AssociationCreateRequest $association_create_request
     * @return GetAssociationResponse
     * @throws \Exception
     */
    protected function createAssociation(AssociationCreateRequest $association_create_request):? GetAssociationResponse
    {
        if (!$association_create_request->isValid()) throw new \Exception('Create Association request not valid. ' . serialize($association_create_request->getErrors()));

        $asscociation_xml = $this->arrayToXMLString($association_create_request->getRequestArray(), 'Association');

        $response = $this->client->XmlAddAssociation(array('identityToken' => $this->getToken(), 'associationXml' => $asscociation_xml));

        if ($this->errorResponse($response->XmlAddAssociationResult)) throw new \Exception('No Association create result received.' . serialize($response));

        $result = $this->xmlToObject($response->XmlAddAssociationResult->Result);

        $this->get_association_response->setResponse($result->Association);

        return $this->get_association_response;
    }


    protected function getAssociationByID(int $association_id)
    {
        $response = $this->client->XmlGetAssociationByID(array('identityToken' => $this->getToken(), 'associationID' => $association_id));

        if ($this->errorResponse($response->XmlGetAssociationByIDResult)) throw new \Exception('No Association result received.' . serialize($response));

        $result = $this->xmlToObject($response->XmlGetAssociationByIDResult->Result);

        $this->get_association_response->setResponse($result->Association);

        return $this->get_association_response;
    }

}