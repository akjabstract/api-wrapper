<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinityProductInstanceObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['site_product_id'] = null;
        $this->response_array['site_product_uid'] = null;
        $this->response_array['product_id'] = null;
        $this->response_array['site_id'] = null;
        $this->response_array['sub_site_id'] = null;
        $this->response_array['cli_id'] = null;
        $this->response_array['quantity'] = null;
        $this->response_array['nominal_code_id'] = null;
        $this->response_array['suppress_billing'] = null;
        $this->response_array['start_date'] = null;
        $this->response_array['end_date'] = null;
        $this->response_array['package_id'] = null;
        $this->response_array['enabled'] = null;
        $this->response_array['bill_description'] = null;
        $this->response_array['do_not_bill'] = null;
        $this->response_array['date_entered'] = null;
        $this->response_array['entered_by_affinity_user_id'] = null;
    }

    public function setObject(\stdClass $affinity_response)
    {
        $this->raw_response = $affinity_response;

        $this->response_array['site_product_id'] = $this->integerField($affinity_response->ProductItemID);
        $this->response_array['site_product_uid'] = $this->stringField($affinity_response->ProductItemID);
        $this->response_array['product_id'] = $this->integerField($affinity_response->ProductItemID);
        $this->response_array['site_id'] = $this->integerField($affinity_response->ProductItemID);
        $this->response_array['sub_site_id'] = $this->integerField($affinity_response->ProductItemID);
        $this->response_array['cli_id'] = $this->integerField($affinity_response->ProductItemID);
        $this->response_array['quantity'] = $this->integerField($affinity_response->ProductItemID);
        $this->response_array['nominal_code_id'] = $this->integerField($affinity_response->ProductItemID);
        $this->response_array['suppress_billing'] = $this->booleanField($affinity_response->ProductItemID);
        $this->response_array['start_date'] = $this->carbonDateFromAffinityField($affinity_response->ProductItemID);
        $this->response_array['end_date'] = $this->carbonDateFromAffinityField($affinity_response->ProductItemID);
        $this->response_array['enabled'] = $this->booleanField($affinity_response->ProductItemID);
        $this->response_array['bill_description'] = $this->stringField($affinity_response->ProductItemID);
        $this->response_array['do_not_bill'] = $this->booleanField($affinity_response->ProductItemID);
        $this->response_array['date_entered'] = $this->carbonDateFromAffinityField($affinity_response->ProductItemID);
        $this->response_array['entered_by_affinity_user_id'] = $this->integerField($affinity_response->ProductItemID);
    }

    public function getSuppressBilling():? bool
    {
        return $this->response_array['suppress_billing'];
    }

    public function getStartDate():? Carbon
    {
        return $this->response_array['start_date'];
    }

    public function getEndDate():? Carbon
    {
        return $this->response_array['end_date'];
    }

    public function getEnabled():? bool
    {
        return $this->response_array['enabled'];
    }

    public function getBillDescription():? string
    {
        return $this->response_array['bill_description'];
    }

    public function getDoNotBill():? bool
    {
        return $this->response_array['do_not_bill'];
    }

    public function getDateEntered():? Carbon
    {
        return $this->response_array['date_entered'];
    }

    public function getEnteredByAffinityUserID():? int
    {
        return $this->response_array['entered_by_affinity_user_id'];
    }

    public function getProductID():? int
    {
        return $this->response_array['product_id'];
    }

    public function getSiteProductID():? int
    {
        return $this->response_array['site_product_id'];
    }

    public function getSiteProductUID():? string
    {
        return $this->response_array['site_product_uid'];
    }

    public function getSiteID():? int
    {
        return $this->response_array['site_id'];
    }

    public function getSubSiteID():? int
    {
        return $this->response_array['sub_site_id'];
    }

    public function getCLIID():? int
    {
        return $this->response_array['cli_id'];
    }

    public function getQuantity():? int
    {
        return $this->response_array['quantity'];
    }

    public function getNominalCodeID():? int
    {
        return $this->response_array['nominal_code_id'];
    }
}