<?php namespace AKJAbstract\APIWrapper\Objects;

use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinityAddressObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['address_id'] = null;
        $this->response_array['address_1'] = null;
        $this->response_array['address_2'] = null;
        $this->response_array['address_3'] = null;
        $this->response_array['address_4'] = null;
        $this->response_array['address_5'] = null;
        $this->response_array['address_6'] = null;
        $this->response_array['country'] = null;
        $this->response_array['address_type'] = null;
    }

    public function setObject(\stdClass $affinity_address_response)
    {
        $this->raw_response = $affinity_address_response;

        $this->response_array['address_id'] = $this->integerField($affinity_address_response->AddressID);
        $this->response_array['address_1'] = $this->stringField($affinity_address_response->Address1);
        $this->response_array['address_2'] = $this->stringField($affinity_address_response->Address2);
        $this->response_array['address_3'] = $this->stringField($affinity_address_response->Address3);
        $this->response_array['address_4'] = $this->stringField($affinity_address_response->Address4);
        $this->response_array['address_5'] = $this->stringField($affinity_address_response->Address5);
        $this->response_array['address_6'] = $this->stringField($affinity_address_response->Address6);
        $this->response_array['country'] = $this->stringField($affinity_address_response->Country);
        $this->response_array['address_type'] = $this->stringField($affinity_address_response->AddressType);

        if(isset($affinity_address_response->UserFields)){
            foreach ((array)$affinity_address_response->UserFields as $UserField => $value) {
                $this->response_array['user_fields'][$UserField] = $this->stringField($value);
            }
        }
    }

    public function getAddressID():? int
    {
        return $this->response_array['address_id'];
    }

    public function getAddress1(): string
    {
        return $this->response_array['address_1'];
    }

    public function getAddress2(): string
    {
        return $this->response_array['address_2'];
    }

    public function getAddress3(): string
    {
        return $this->response_array['address_3'];
    }

    public function getAddress4(): string
    {
        return $this->response_array['address_4'];
    }

    public function getAddress5(): string
    {
        return $this->response_array['address_5'];
    }

    public function getAddress6(): string
    {
        return $this->response_array['address_6'];
    }

    public function getCountry():? string
    {
        return $this->response_array['country'];
    }

    public function getAddressType():? string
    {
        return $this->response_array['address_type'];
    }

}