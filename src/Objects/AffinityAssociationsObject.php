<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinityAssociationsObject extends AbstractAffinityObject implements AffinityObjectInterface
{

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['association_id'] = null;
        $this->response_array['item_uid'] = null;
        $this->response_array['item_connection_type'] = null;
        $this->response_array['associated_uid'] = null;
        $this->response_array['associated_connection_type'] = null;

        // who knows what these are ?
        $this->response_array['email_bill'] = null;
        $this->response_array['email_promotions'] = null;
        $this->response_array['email_sub_site_statements'] = null;
        $this->response_array['email_site_user_statements'] = null;
    }

    public function setObject(\stdClass $affinity_association_response)
    {
        $this->raw_response = $affinity_association_response;

        if (isset($affinity_association_response->AssociationID)) {
            $this->response_array['association_id'] = $this->stringField($affinity_association_response->AssociationID);
            $this->response_array['item_uid'] = $this->stringField($affinity_association_response->ItemUID);
            $this->response_array['item_connection_type'] = $this->stringField($affinity_association_response->ItemConnectionType);
            $this->response_array['associated_uid'] = $this->stringField($affinity_association_response->AssociatedUID);
            $this->response_array['associated_connection_type'] = $this->stringField($affinity_association_response->AssociatedConnectionType);

            $this->response_array['email_bill'] = $this->integerField($affinity_association_response->EmailBill);
            $this->response_array['email_promotions'] = $this->integerField($affinity_association_response->EmailPromotions);
            $this->response_array['email_sub_site_statements'] = $this->integerField($affinity_association_response->EmailSubSiteStatements);
            $this->response_array['email_site_user_statements'] = $this->integerField($affinity_association_response->EmailSiteUserStatements);

        }
    }

    public function getAssociationID():? int
    {
        return $this->response_array['association_id'];
    }

    public function getItemUID(): string
    {
        return $this->response_array['item_uid'];
    }

    public function getItemConnectionType():? string
    {
        return $this->response_array['item_connection_type'];
    }

    public function getAssociatedUID():? string
    {
        return $this->response_array['associated_uid'];
    }

    public function getAssociatedConnectionType():? string
    {
        return $this->response_array['associated_connection_type'];
    }
}