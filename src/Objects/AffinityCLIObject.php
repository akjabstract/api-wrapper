<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinityCLIObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['cli_id'] = null;
        $this->response_array['cli_uid'] = null;
        $this->response_array['cli_number'] = null;
        $this->response_array['description'] = false;
        $this->response_array['site_id'] = null;
        $this->response_array['sub_site_id'] = null;
        $this->response_array['live'] = false;
        $this->response_array['date_live'] = null;
        $this->response_array['date_entered'] = null;
        $this->response_array['start_date'] = null;
        $this->response_array['end_date'] = null;
        $this->response_array['user_fields'] = [];
        $this->response_array['user_field_service_type'] = null;
        $this->response_array['user_field_service_provider'] = null;
        $this->response_array['user_field_service_provider_account_number'] = null;
    }

    public function setObject(\stdClass $affinity_cli_response)
    {
        $this->raw_response = $affinity_cli_response;

        $this->response_array['cli_id'] = $this->integerField($affinity_cli_response->CLIID);
        $this->response_array['cli_uid'] = $this->stringField($affinity_cli_response->CLIUID);
        $this->response_array['cli_number'] = $this->stringField($affinity_cli_response->CLINumber);
        $this->response_array['description'] = $this->stringField($affinity_cli_response->Description);
        $this->response_array['site_id'] = $this->integerField($affinity_cli_response->SiteID);
        $this->response_array['sub_site_id'] = $this->integerField($affinity_cli_response->SubSiteID);
        $this->response_array['live'] = $this->booleanField($affinity_cli_response->Live);
        $this->response_array['date_live'] = $this->carbonDateFromAffinityField($affinity_cli_response->DateLive);
        $this->response_array['date_entered'] = $this->carbonDateFromAffinityField($affinity_cli_response->DateEntered);
        $this->response_array['start_date'] = $this->carbonDateFromAffinityField($affinity_cli_response->StartDate);
        $this->response_array['end_date'] = $this->carbonDateFromAffinityField($affinity_cli_response->EndDate);

        foreach ((array)$affinity_cli_response->UserFields as $UserField => $value) {
            $this->response_array['user_fields'][$UserField] = $this->stringField($value);
        }
    }

    public function getCLIID():? int
    {
        return $this->response_array['cli_id'];
    }

    public function getCLIUID(): string
    {
        return $this->response_array['cli_uid'];
    }

    public function getCLINumber():? string
    {
        return $this->response_array['cli_number'];
    }

    public function getDescription():? string
    {
        return $this->response_array['description'];
    }

    public function getSiteID():? int
    {
        return $this->response_array['site_id'];
    }

    public function getSubSiteID():? int
    {
        return $this->response_array['sub_site_id'];
    }

    public function getLive():? bool
    {
        return $this->response_array['live'];
    }

    public function getDateLive():? Carbon
    {
        return $this->response_array['date_live'];
    }

    public function getDateEntered():? Carbon
    {
        return $this->response_array['date_entered'];
    }

    public function getStartDate():? Carbon
    {
        return $this->response_array['start_date'];
    }

    public function getEndDate():? Carbon
    {
        return $this->response_array['end_date'];
    }

    public function getUserFieldServiceProvider():? string
    {
        return $this->response_array['user_fields']['Service_Provider'];
    }

    public function getUserFieldServiceProviderAccountNumber():? string
    {
        return $this->response_array['user_fields']['Service_Provider_Account_Number'];
    }

    public function getUserFieldServiceType():? string
    {
        return $this->response_array['user_fields']['Service_Type'];
    }
}