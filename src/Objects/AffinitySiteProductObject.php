<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinitySiteProductObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {

        $this->response_array['site_product_item_id'] = null;
        $this->response_array['site_product_item_uid'] = null;
        $this->response_array['product_item_id'] = null;
        $this->response_array['site_id'] = null;
        $this->response_array['sub_site_id'] = null;
        $this->response_array['quantity'] = null;
        $this->response_array['reference1'] = null;
        $this->response_array['start_date'] = null;
        $this->response_array['end_date'] = null;
        $this->response_array['crm_user_id'] = null;
        $this->response_array['refund'] = null;
        $this->response_array['enabled'] = null;
    }

    public function setObject(\stdClass $affinity_response)
    {
        $this->raw_response = $affinity_response;

        $this->response_array['site_product_item_id'] = $this->integerField($affinity_response->SiteProductItemID);
        $this->response_array['site_product_item_uid'] = $this->stringField($affinity_response->SiteProductItemUID);
        $this->response_array['product_item_id'] = $this->integerField($affinity_response->ProductItemID);
        $this->response_array['site_id'] = $this->integerField($affinity_response->SiteID);
        $this->response_array['sub_site_id'] = $this->integerField($affinity_response->SubSiteID);
        $this->response_array['quantity'] = $this->integerField($affinity_response->Quantity);
        $this->response_array['reference1'] = $this->stringField($affinity_response->Reference1);
        $this->response_array['start_date'] = $this->carbonDateFromAffinityField($affinity_response->StartDate);
        $this->response_array['end_date'] = $this->carbonDateFromAffinityField($affinity_response->EndDate);
        $this->response_array['crm_user_id'] = $this->integerField($affinity_response->CRMUserID);
        $this->response_array['refund'] = $this->booleanField($affinity_response->Refund);
        $this->response_array['enabled'] = $this->booleanField($affinity_response->Enabled);
    }

    public function getSiteProductItemID():? int
    {
        return $this->response_array['site_product_item_id'];
    }

    public function getProductItemID():? int
    {
        return $this->response_array['product_item_id'];
    }

    public function getSiteProductItemUID():? string
    {
        return $this->response_array['site_product_item_uid'];
    }

    public function getSiteID():? int
    {
        return $this->response_array['site_id'];
    }

    public function getSubSiteID():? int
    {
        return $this->response_array['sub_site_id'];
    }

    public function getQuantity():? int
    {
        return $this->response_array['quantity'];
    }

    public function getReference1():? string
    {
        return $this->response_array['reference1'];
    }

    public function getStartDate():? Carbon
    {
        return $this->response_array['start_date'];
    }

    public function getEndDate():? Carbon
    {
        return $this->response_array['end_date'];
    }

    public function getCRMUserID():? int
    {
        return $this->response_array['crm_user_id'];
    }

    public function getEnabled():? bool
    {
        return $this->response_array['enabled'];
    }

    public function getRefund():? bool
    {
        return $this->response_array['refund'];
    }

}