<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;

class AffinitySiteBroadbandObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['site_broadband_id'] = null;
        $this->response_array['site_broadband_uid'] = null;
        $this->response_array['site_id'] = null;
        $this->response_array['cli_id'] = null;
        $this->response_array['username'] = null;
        $this->response_array['password'] = null;
    }

    public function setObject(\stdClass $affinity_response)
    {
        $this->raw_response = $affinity_response;

        $this->response_array['site_broadband_id'] = $this->integerField($affinity_response->SiteBroadBandID);
        $this->response_array['site_broadband_uid'] = $this->stringField($affinity_response->SiteBroadBandUID);
        $this->response_array['site_id'] = $this->integerField($affinity_response->SiteID);
        $this->response_array['cli_id'] = $this->integerField($affinity_response->CliID);
        $this->response_array['username'] = $this->stringField($affinity_response->Username);
        $this->response_array['password'] = $this->stringField($affinity_response->Password);
    }

    public function getSiteID():? int
    {
        return $this->response_array['site_id'];
    }

    public function getSiteBroadbandID():? int
    {
        return $this->response_array['site_broadband_id'];
    }

    public function getSiteBroadbandUID():? int
    {
        return $this->response_array['site_broadband_uid'];
    }

    public function getCLIID():? int
    {
        return $this->response_array['cli_id'];
    }

    public function getUsername():? string
    {
        return $this->response_array['username'];
    }

    public function getPassword():? string
    {
        return $this->response_array['password'];
    }

}