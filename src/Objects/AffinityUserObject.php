<?php namespace AKJAbstract\APIWrapper\Objects;

class AffinityUserObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['user_id'] = null;
        $this->response_array['user_uid'] = null;
        $this->response_array['team_id'] = null;
        $this->response_array['username'] = null;
        $this->response_array['disabled'] = null;
    }

    public function setObject(\stdClass $affinity_response)
    {
        $this->raw_response = $affinity_response;

        $this->response_array['user_id'] = $this->integerField($affinity_response->UserID);
        $this->response_array['user_uid'] = $this->integerField($affinity_response->UserUID);
        $this->response_array['team_id'] = $this->integerField($affinity_response->TeamId);
        $this->response_array['username'] = $this->stringField($affinity_response->Username);
        $this->response_array['disabled'] = $this->booleanField($affinity_response->Disabled);
    }

    public function getUserID():? int
    {
        return $this->response_array['user_id'];
    }

    public function getUserUID():? int
    {
        return $this->response_array['user_uid'];
    }

    public function getTeamID():? int
    {
        return $this->response_array['team_id'];
    }

    public function getUsername():? string
    {
        return $this->response_array['username'];
    }

    public function getDisabled():? bool
    {
        return $this->response_array['disabled'];
    }
}