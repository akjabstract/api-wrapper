<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinityTicketObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['ticket_id'] = null;
        $this->response_array['ticket_uid'] = null;
        $this->response_array['summary'] = null;
        $this->response_array['details'] = null;
        $this->response_array['priority'] = null;
        $this->response_array['date_due'] = null;
        $this->response_array['entered_by_id'] = null;
        $this->response_array['owner_id'] = null;
        $this->response_array['team_id'] = null;
        $this->response_array['category'] = null;
        $this->response_array['sub_category'] = null;
        $this->response_array['status'] = null;
        $this->response_array['connected_to'] = null;
        $this->response_array['connection_type'] = null;
        $this->response_array['connected_to_child'] = null;
        $this->response_array['connection_type_child'] = null;
        $this->response_array['user_fields'] = [];
    }

    public function setObject(\stdClass $affinity_ticket_response)
    {
        $this->raw_response = $affinity_ticket_response;

        if (isset($affinity_ticket_response->TicketID)) {
            $this->response_array['ticket_id'] = $this->integerField($affinity_ticket_response->TicketID);
            $this->response_array['ticket_uid'] = $this->stringField($affinity_ticket_response->TicketUID);
            $this->response_array['summary'] = $this->stringField($affinity_ticket_response->Summary);
            $this->response_array['details'] = $this->stringField($affinity_ticket_response->Details);
            $this->response_array['priority'] = $this->integerField($affinity_ticket_response->Priority);
            $this->response_array['date_due'] = $this->carbonDateFromAffinityField($affinity_ticket_response->DateDue);
            $this->response_array['entered_by_id'] = $this->integerField($affinity_ticket_response->EnteredByID);
            $this->response_array['owner_id'] = $this->integerField($affinity_ticket_response->OwnerID);
            $this->response_array['team_id'] = $this->integerField($affinity_ticket_response->TeamID);
            $this->response_array['category'] = $this->stringField($affinity_ticket_response->Category);
            $this->response_array['sub_category'] = $this->stringField($affinity_ticket_response->SubCategory);
            $this->response_array['status'] = $this->stringField($affinity_ticket_response->Status);
            $this->response_array['date_created'] = $this->carbonDateFromAffinityField(isset($affinity_ticket_response->DateEntered) ? $affinity_ticket_response->DateEntered : $affinity_ticket_response->DateCreated);
            $this->response_array['connected_to'] = $this->stringField($affinity_ticket_response->ConnectedTo);
            $this->response_array['connection_type'] = $this->stringField($affinity_ticket_response->ConnectionType);
            $this->response_array['connected_to_child'] = $this->stringField($affinity_ticket_response->ConnectedToChild);
            $this->response_array['connection_type_child'] = $this->stringField($affinity_ticket_response->ConnectionTypeChild);

            foreach ((array)$affinity_ticket_response->UserFields as $UserField => $value) {
                $this->response_array['user_fields'][$UserField] = $this->stringField($value);
            }
        }
    }

    public function getTicketID():? int
    {
        return $this->response_array['ticket_id'];
    }

    public function getTicketUID(): string
    {
        return $this->response_array['ticket_uid'];
    }

    public function getSummary():? string
    {
        return $this->response_array['summary'];
    }

    public function getDetails():? string
    {
        return $this->response_array['details'];
    }

    public function getPriority():? int
    {
        return $this->response_array['priority'];
    }

    public function getDateDue():? Carbon
    {
        return $this->response_array['date_due'];
    }

    public function getEnteredByID():? int
    {
        return $this->response_array['entered_by_id'];
    }

    public function getOwnerID():? int
    {
        return $this->response_array['owner_id'];
    }

    public function getTeamID(): int
    {
        return $this->response_array['team_id'];
    }

    public function getCategory():? string
    {
        return $this->response_array['category'];
    }

    public function getSubCategory():? string
    {
        return $this->response_array['sub_category'];
    }

    public function getStatus():? string
    {
        return $this->response_array['status'];
    }

    public function getDateCreated():? Carbon
    {
        return $this->response_array['date_created'];
    }

    public function getConnectedTo():? string
    {
        return $this->response_array['connected_to'];
    }

    public function getConnectionType():? string
    {
        return $this->response_array['connection_type'];
    }

    public function getConnectedToChild():? string
    {
        return $this->response_array['connected_to_child'];
    }

    public function getConnectionTypeChild():? string
    {
        return $this->response_array['connection_type_child'];
    }

    public function getUserField(string $user_field):? string
    {
        return $this->response_array['user_fields'][$user_field];
    }

    public function getUserFieldDateConfirmed():?Carbon
    {
        try {
            return Carbon::createFromFormat('Y-m-d H:i:s', substr(str_replace('T', ' ', $this->response_array['user_fields']['Date_Confirmed']), 0, 19));
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getUserFieldRenewalStartDate():?Carbon
    {
        try {
            return Carbon::createFromFormat('Y-m-d H:i:s', substr(str_replace('T', ' ', $this->response_array['user_fields']['Renewal_Start_Date']), 0, 19));
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getUserFieldCustomerOrTicketStatus():?string
    {
        return is_string($this->response_array['user_fields']['Customer_or_Ticket_Status']) ? $this->response_array['user_fields']['Customer_or_Ticket_Status'] : null;
    }

    public function getUserFieldApplySiteTariff():?bool
    {
        return is_string($this->response_array['user_fields']['Apply_Site_Tariff']) ? $this->response_array['user_fields']['Apply_Site_Tariff'] == '1': false;
    }

    public function getUserFieldCompletedBy():?string
    {
        return is_string($this->response_array['user_fields']['Completed_By']) ? $this->response_array['user_fields']['Completed_By'] : null;
    }

    public function getUserFieldReasonCode():?string
    {
        return is_string($this->response_array['user_fields']['Reason_Code']) ? $this->response_array['user_fields']['Reason_Code'] : null;
    }

    public function getUserFieldStayingWithUsOption():?string
    {
        return is_string($this->response_array['user_fields']['Staying_with_us_option']) ? $this->response_array['user_fields']['Staying_with_us_option'] : null;
    }
}