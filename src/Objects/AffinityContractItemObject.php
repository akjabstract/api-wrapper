<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinityContractItemObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['contract_item_id'] = null;
        $this->response_array['contract_item_uid'] = null;
        $this->response_array['contract_id'] = null;
        $this->response_array['connection_uid'] = null;
        $this->response_array['connection_type'] = null;
        $this->response_array['start_date'] = null;
        $this->response_array['end_date'] = null;
        $this->response_array['date_added'] = null;
        $this->response_array['termination_date'] = null;
    }

    public function setObject(\stdClass $affinity_contract_id_response)
    {
        $this->raw_response = $affinity_contract_id_response;

        $this->response_array['contract_item_id'] = $this->integerField($affinity_contract_id_response->ContractItemID);
        $this->response_array['contract_item_uid'] = $this->stringField($affinity_contract_id_response->ContractItemUID);
        $this->response_array['contract_id'] = $this->integerField($affinity_contract_id_response->ContractID);
        $this->response_array['connection_uid'] = $this->stringField($affinity_contract_id_response->ConnectedTo);
        $this->response_array['connection_type'] = $this->stringField($affinity_contract_id_response->ConnectionType);
        $this->response_array['start_date'] = $this->carbonDateFromAffinityField($affinity_contract_id_response->StartDate);
        $this->response_array['end_date'] = $this->carbonDateFromAffinityField($affinity_contract_id_response->EndDate);
        $this->response_array['date_added'] = $this->carbonDateFromAffinityField($affinity_contract_id_response->DateAdded);
        $this->response_array['termination_date'] = $this->carbonDateFromAffinityField($affinity_contract_id_response->TerminationDate);
    }

    public function getContractItemID():? int
    {
        return $this->response_array['contract_item_id'];
    }

    public function getContractItemUID():? string
    {
        return $this->response_array['contract_item_uid'];
    }

    public function getContractID():? int
    {
        return $this->response_array['contract_id'];
    }

    public function getConnectionUID():? string
    {
        return $this->response_array['connection_uid'];
    }

    public function getConnectionType():? string
    {
        return $this->response_array['connection_type'];
    }

    public function getStartDate():? Carbon
    {
        return $this->response_array['start_date'];
    }

    public function getEndDate():? Carbon
    {
        return $this->response_array['end_date'];
    }

    public function getDateAdded():? Carbon
    {
        return $this->response_array['date_added'];
    }

    public function getTerminationDate():? Carbon
    {
        return $this->response_array['termination_date'];
    }
}