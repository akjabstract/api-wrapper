<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinityTransactionObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['site_id'] = null;
        $this->response_array['transact_id'] = null;
        $this->response_array['transaction_date'] = null;
        $this->response_array['payment_method'] = null;
        $this->response_array['credits'] = null;
        $this->response_array['debits'] = null;
        $this->response_array['gross'] = null;
        $this->response_array['net'] = null;
        $this->response_array['vat'] = null;
        $this->response_array['invoice_date'] = null;
        $this->response_array['payment_ref'] = null;
        $this->response_array['description'] = null;
        $this->response_array['part_paid'] = null;
        $this->response_array['nominal_code'] = null;
        $this->response_array['vat_rate'] = null;
        $this->response_array['posting_date'] = null;
        $this->response_array['invoice_number'] = null;
        $this->response_array['transaction_code'] = null;
        $this->response_array['balance'] = null;
    }

    public function setObject(\stdClass $affinity_transaction_response)
    {
        $this->raw_response = $affinity_transaction_response;

        if (isset($affinity_transaction_response->SiteID)) {
            $this->response_array['site_id'] = $this->integerField($affinity_transaction_response->SiteID);
            $this->response_array['transact_id'] = $this->integerField($affinity_transaction_response->TransactID);
            $this->response_array['transaction_date'] = $this->carbonDateFromAffinityField($affinity_transaction_response->TransactionDate);
            $this->response_array['payment_method'] = $this->stringField($affinity_transaction_response->PaymentMethod);
            $this->response_array['credits'] = $this->floatField($affinity_transaction_response->Credits);
            $this->response_array['debits'] = $this->floatField($affinity_transaction_response->Debits);
            $this->response_array['gross'] = $this->floatField($affinity_transaction_response->Gross);
            $this->response_array['net'] = $this->floatField($affinity_transaction_response->Net);
            $this->response_array['vat'] = $this->floatField($affinity_transaction_response->VAT);
            $this->response_array['invoice_date'] = $this->carbonDateFromAffinityField($affinity_transaction_response->InvoiceDate);
            $this->response_array['payment_ref'] = $this->stringField($affinity_transaction_response->PaymentRef);
            $this->response_array['description'] = $this->stringField($affinity_transaction_response->Description);
            $this->response_array['part_paid'] = $this->floatField($affinity_transaction_response->PartPaid);
            $this->response_array['nominal_code'] = $this->integerField($affinity_transaction_response->NominalCode);
            $this->response_array['vat_rate'] = $this->integerField($affinity_transaction_response->VATRate);
            $this->response_array['posting_date'] = $this->carbonDateFromAffinityField($affinity_transaction_response->PostingDate);
            $this->response_array['invoice_number'] = $this->integerField($affinity_transaction_response->InvoiceNo);
            $this->response_array['transaction_code'] = $this->stringField($affinity_transaction_response->TransactionCode);
            $this->response_array['on_dispute'] = $this->booleanField($affinity_transaction_response->OnDispute);
            $this->response_array['payment_terms'] = $this->integerField($affinity_transaction_response->PaymentTerms);
            $this->response_array['invoice_due_date'] = $this->carbonDateFromAffinityField($affinity_transaction_response->InvoiceDueDate);
        }
    }

    public function getSiteID(): ?int
    {
        return $this->response_array['site_id'];
    }

    public function getTransactID(): ?int
    {
        return $this->response_array['transact_id'];
    }

    public function getTransactionDate(): ?Carbon
    {
        return $this->response_array['transaction_date'];
    }

    public function getPaymentMethod(): ?string
    {
        return $this->response_array['payment_method'];
    }

    public function getCredits(): ?float
    {
        return $this->response_array['credits'];
    }

    public function getDebits(): ?float
    {
        return $this->response_array['debits'];
    }

    public function getGross(): ?float
    {
        return $this->response_array['gross'];
    }

    public function getNet(): ?float
    {
        return $this->response_array['net'];
    }

    public function getVat(): ?float
    {
        return $this->response_array['vat'];
    }

    public function getInvoiceDate(): ?Carbon
    {
        return $this->response_array['invoice_date'];
    }

    public function getPaymentRef(): ?string
    {
        return $this->response_array['payment_ref'];
    }

    public function getDescription(): ?string
    {
        return $this->response_array['description'];
    }

    public function getTransactionCode(): ?string
    {
        return $this->response_array['transaction_code'];
    }

    public function getPartPaid(): ?float
    {
        return $this->response_array['part_paid'];
    }

    public function getNominalCode(): ?int
    {
        return $this->response_array['nominal_code'];
    }

    public function getInvoiceNumber(): ?int
    {
        return $this->response_array['invoice_number'];
    }

    public function getVatRate(): ?int
    {
        return $this->response_array['vat_rate'];
    }

    public function getPostingDate(): ?Carbon
    {
        return $this->response_array['posting_date'];
    }

    public function getOnDispute(): ?Bool
    {
        return $this->response_array['on_dispute'];
    }

    public function getPaymentTerms(): ?Int
    {
        return $this->response_array['payment_terms'];
    }

    public function getInvoiceDueDate(): ?Carbon
    {
        return $this->response_array['invoice_due_date'];
    }
}