<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinityAssignedDealerTariffObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['assigned_tariff_id'] = null;
        $this->response_array['scheme_id'] = null;
        $this->response_array['service'] = null;
        $this->response_array['start_date'] = null;
        $this->response_array['end_date'] = null;
        $this->response_array['company_id'] = null;
    }

    public function setObject(\stdClass $affinity_response)
    {
        $this->raw_response = $affinity_response;

        $this->response_array['assigned_tariff_id'] = $this->integerField($affinity_response->ID);
        $this->response_array['service'] = $this->integerField($affinity_response->Service);
        $this->response_array['scheme_id'] = $this->integerField($affinity_response->SchemeID);
        $this->response_array['start_date'] = $this->carbonDateFromAffinityField($affinity_response->StartDate);
        $this->response_array['end_date'] = $this->carbonDateFromAffinityField($affinity_response->EndDate);
        $this->response_array['company_id'] = $this->integerField($affinity_response->CompanyID);
    }

    public function getAssignedTariffID():? int
    {
        return $this->response_array['assigned_tariff_id'];
    }

    public function getSchemeID():? int
    {
        return $this->response_array['scheme_id'];
    }

    public function getService():? int
    {
        return $this->response_array['service'];
    }

    public function getStartDate():? Carbon
    {
        return $this->response_array['start_date'];
    }

    public function getEndDate():? Carbon
    {
        return $this->response_array['end_date'];
    }

    public function getCompanyID():? int
    {
        return $this->response_array['company_id'];
    }
}