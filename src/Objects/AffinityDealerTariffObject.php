<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinityDealerTariffObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['scheme_id'] = null;
        $this->response_array['dealer_id'] = null;
        $this->response_array['service'] = null;
        $this->response_array['scheme_name'] = null;
        $this->response_array['live'] = null;
        $this->response_array['start_date'] = null;
        $this->response_array['currency_code'] = null;
    }

    public function setObject(\stdClass $affinity_team_response)
    {
        $this->raw_response = $affinity_team_response;

        $this->response_array['scheme_id'] = $this->integerField($affinity_team_response->SchemeID);
        $this->response_array['dealer_id'] = $this->integerField($affinity_team_response->DealerID);
        $this->response_array['service'] = $this->integerField($affinity_team_response->Service);
        $this->response_array['scheme_name'] = $this->stringField($affinity_team_response->SchemeName);
        $this->response_array['live'] = $this->booleanField($affinity_team_response->Live);
        $this->response_array['start_date'] = $this->carbonDateFromAffinityField($affinity_team_response->StartDate);
        $this->response_array['currency_code'] = $this->stringField($affinity_team_response->CurrencyCode);
    }

    public function getSchemeID():? int
    {
        return $this->response_array['scheme_id'];
    }

    public function getDealerID():? int
    {
        return $this->response_array['dealer_id'];
    }

    public function getService():? int
    {
        return $this->response_array['service'];
    }

    public function getSchemeName():? string
    {
        return $this->response_array['scheme_name'];
    }

    public function getLive():? bool
    {
        return $this->response_array['live'];
    }

    public function getStartDate():? Carbon
    {
        return $this->response_array['start_date'];
    }

    public function getCurrencyCode():? string
    {
        return $this->response_array['currency_code'];
    }
}