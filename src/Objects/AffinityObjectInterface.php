<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Responses\AbstractResponse;
use AKJAbstract\APIWrapper\Responses\AbstractResponseInterface;

interface AffinityObjectInterface
{
    public function setObject(\stdClass $affinity_response);

    public function getResponseAsArray(): array;

    public function getResponseAsObject(): \stdClass;

    public function getRawResponse(): \stdClass;
}