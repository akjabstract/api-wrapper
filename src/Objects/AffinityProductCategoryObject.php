<?php namespace AKJAbstract\APIWrapper\Objects;

use Carbon\Carbon;
use AKJAbstract\APIWrapper\Traits\DatesTrait;

class AffinityProductCategoryObject extends AbstractAffinityObject implements AffinityObjectInterface
{
    use DatesTrait;

    protected $raw_response;

    protected $response_array = [];

    public function __construct()
    {
        $this->response_array['category_id'] = null;
        $this->response_array['category_uid'] = null;
        $this->response_array['description'] = null;
        $this->response_array['category_group_id'] = null;
        $this->response_array['last_edited_date'] = null;
        $this->response_array['affinity_user_id'] = null;
        $this->response_array['enabled'] = null;
        $this->response_array['parent_category_id'] = null;
        $this->response_array['nominal_code_id'] = null;
    }

    public function setObject(\stdClass $affinity_response)
    {
        $this->raw_response = $affinity_response;

        $this->response_array['category_id'] = $this->integerField($affinity_response->CategoryID);
        $this->response_array['category_uid'] = $this->stringField($affinity_response->CategoryUID);
        $this->response_array['description'] = $this->stringField($affinity_response->CategoryDescription);
        $this->response_array['category_group_id'] = $this->integerField($affinity_response->CategoryGroupID);
        $this->response_array['last_edited_date'] = $this->carbonDateFromAffinityField($affinity_response->LastEdited);
        $this->response_array['affinity_user_id'] = $this->integerField($affinity_response->CRMUserID);
        $this->response_array['enabled'] = $this->booleanField($affinity_response->Enabled);
        $this->response_array['parent_category_id'] = $this->integerField($affinity_response->ParentCategoryID);
        $this->response_array['nominal_code_id'] = $this->integerField($affinity_response->NominalCodeID);
    }

    public function getCategoryID():? int
    {
        return $this->response_array['category_id'];
    }

    public function getCategoryUID():? string
    {
        return $this->response_array['category_uid'];
    }

    public function getCategoryGroupID():? int
    {
        return $this->response_array['category_group_id'];
    }

    public function getAffinityUserID():? int
    {
        return $this->response_array['affinity_user_id'];
    }

    public function getDescription():? string
    {
        return $this->response_array['description'];
    }

    public function getEnabled():? bool
    {
        return $this->response_array['enabled'];
    }

    public function getParentCategoryID():? int
    {
        return $this->response_array['parent_category_id'];
    }

    public function getNominalCodeID():? int
    {
        return $this->response_array['nominal_code_id'];
    }

    public function getLastEditedDate():? Carbon
    {
        return $this->response_array['last_edited_date'];
    }
}